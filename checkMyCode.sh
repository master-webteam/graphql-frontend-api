#!/usr/bin/env bash
php -d memory_limit=4G ./vendor/bin/phpstan analyse -l "7" -c ./phpstan.neon ./src
./vendor/bin/ecs check ./src
