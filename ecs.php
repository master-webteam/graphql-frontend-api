<?php

declare(strict_types=1);

use PhpCsFixer\Fixer\ClassNotation\ClassAttributesSeparationFixer;
use PhpCsFixer\Fixer\ClassNotation\NoBlankLinesAfterClassOpeningFixer;
use PhpCsFixer\Fixer\FunctionNotation\MethodArgumentSpaceFixer;
use PhpCsFixer\Fixer\Operator\NotOperatorWithSuccessorSpaceFixer;
use PhpCsFixer\Fixer\Phpdoc\GeneralPhpdocAnnotationRemoveFixer;
use PhpCsFixer\Fixer\Phpdoc\NoSuperfluousPhpdocTagsFixer;
use PhpCsFixer\Fixer\Phpdoc\PhpdocLineSpanFixer;
use PhpCsFixer\Fixer\PhpUnit\PhpUnitSetUpTearDownVisibilityFixer;
use PhpCsFixer\Fixer\Whitespace\MethodChainingIndentationFixer;
use Symplify\CodingStandard\Fixer\ArrayNotation\StandaloneLineInMultilineArrayFixer;
use Symplify\CodingStandard\Fixer\ArrayNotation\ArrayListItemNewlineFixer;
use Symplify\CodingStandard\Fixer\ArrayNotation\ArrayOpenerAndCloserNewlineFixer;
use Symplify\CodingStandard\Fixer\Commenting\RemoveUselessDefaultCommentFixer;
use Symplify\CodingStandard\Fixer\LineLength\LineLengthFixer;
use Symplify\CodingStandard\Fixer\Spacing\MethodChainingNewlineFixer;
use Symplify\EasyCodingStandard\ValueObject\Set\SetList;
use PhpCsFixer\Fixer\ClassNotation\ClassDefinitionFixer;
use PhpCsFixer\Fixer\ClassNotation\OrderedClassElementsFixer;
use PhpCsFixer\Fixer\Whitespace\ArrayIndentationFixer;
use PhpCsFixer\Fixer\ArrayNotation\TrimArraySpacesFixer;
use PhpCsFixer\Fixer\ControlStructure\TrailingCommaInMultilineFixer;
use Symplify\CodingStandard\Fixer\Spacing\StandaloneLinePromotedPropertyFixer;
use Symplify\EasyCodingStandard\Config\ECSConfig;

return static function (ECSConfig $ecsConfig): void {

    $ecsConfig->indentation('spaces');
    $ecsConfig->fileExtensions(['php', 'phpt']);
    $ecsConfig->import(SetList::CLEAN_CODE);
    $ecsConfig->import(SetList::NAMESPACES);
    $ecsConfig->import(SetList::STRICT);
    $ecsConfig->import(SetList::SPACES);
    $ecsConfig->import(SetList::COMMON);
    $ecsConfig->import(SetList::SYMPLIFY);
    $ecsConfig->import(SetList::DOCBLOCK);
    $ecsConfig->import(SetList::CONTROL_STRUCTURES);
    $ecsConfig->import(SetList::ARRAY);

    $ecsConfig->skip(skips: [
        NotOperatorWithSuccessorSpaceFixer::class => null,
        RemoveUselessDefaultCommentFixer::class,
        ClassDefinitionFixer::class => null,
        NoSuperfluousPhpdocTagsFixer::class => null,
        PhpdocLineSpanFixer::class => null,
        PhpUnitSetUpTearDownVisibilityFixer::class => null,
        LineLengthFixer::class => null,
        MethodChainingIndentationFixer::class => null,
        MethodChainingNewlineFixer::class => null,
        ClassAttributesSeparationFixer::class => null,
        NoBlankLinesAfterClassOpeningFixer::class => null,
        OrderedClassElementsFixer::class => null,
        StandaloneLineInMultilineArrayFixer::class => null,
        ArrayIndentationFixer::class => null,
        ArrayListItemNewlineFixer::class => null,
        ArrayOpenerAndCloserNewlineFixer::class => null,
        MethodArgumentSpaceFixer::class => null,
        StandaloneLinePromotedPropertyFixer::class => null,
        GeneralPhpdocAnnotationRemoveFixer::class => null,
        TrailingCommaInMultilineFixer::class => null,
        PHP_CodeSniffer\Standards\Generic\Sniffs\CodeAnalysis\AssignmentInConditionSniff::class => null,
        TrimArraySpacesFixer::class => null
    ]);
};
