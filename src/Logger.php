<?php

declare(strict_types=1);

/** @noinspection PhpUnused */

namespace GraphQLFrontApi;

use DateTime;
use GraphQLFrontApi\Communication\CommunicationUtil;
use GraphQLFrontApi\Communication\Entities\SendGraphQLRequestParameters;
use GraphQLFrontApi\Debug\DebugObject;
use GraphQLFrontApi\Exceptions\Communication\GraphQLApiInternalException;
use GraphQLFrontApi\Exceptions\GraphQLException;
use JsonException;
use Nette\Caching\Cache;
use Nette\Caching\Storage;
use RuntimeException;
use function Sentry\captureException;
use Sentry\EventId;
use Sentry\State\Scope;
use function Sentry\withScope;

class Logger {

    // Directory for the logs [DI]
    public string $logPath;

    // App version [DI]
    private string $version;

    // Ignored outputs [DI]
    private ?array $ignored;

    // Environment name [DI]
    private string|null $environment;

    // If log everything [DI]
    private bool $logDebugMessages;

    // Use short log format (f.e. sending LOGS for BE team) [DI]
    private bool $localLogSimplified;

    // Seconds to message next exception [DI]
    private int|null $spikeTimeoutSeconds;

    // Separator [CONST]
    private const string Line = '----------------------------------------END OF TRANSACTION----------------------------------------';

    // Tag for cache and spike protection
    private const string spikeProtectionCacheTag = 'spikeProtection';

    /** @noinspection PhpUnused */
    public function setSpikeTimeoutSeconds(int|null $spikeTimeoutSeconds): void {

        $this->spikeTimeoutSeconds = $spikeTimeoutSeconds;
    }

    /** @noinspection PhpUnused */
    public function setLogPath(string $logPath): void {

        $this->logPath = $logPath;
    }

    /** @noinspection PhpUnused */
    public function setVersion(string $version): void {

        $this->version = $version;
    }

    /** @noinspection PhpUnused */
    public function setIgnored(?array $ignored): void {

        $this->ignored = $ignored;
    }

    /** @noinspection PhpUnused */
    public function setEnvironment(string|null $environment): void {

        $this->environment = $environment;
    }

    /** @noinspection PhpUnused */
    public function setLogDebugMessages(bool $logDebugMessages): void {

        $this->logDebugMessages = $logDebugMessages;
    }

    /** @noinspection PhpUnused */
    public function setLocalLogSimplified(bool $localLogSimplified): void {

        $this->localLogSimplified = $localLogSimplified;
    }

    public function __construct(private readonly Storage $cache) {
    }

    /**
     * Initialize log directory
     * Creates dir if not exists
     */
    private function setUpLogDir(): void {

        $exceptions = "{$this->logPath}/exceptions";
        if (!file_exists($exceptions) && !mkdir($exceptions, 0777, true) && !is_dir($exceptions)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $exceptions));
        }

        $debug = "{$this->logPath}/debug";
        if (!file_exists($debug) && !mkdir($debug, 0777, true) && !is_dir($debug)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $debug));
        }
    }

    /**
     * @param GraphQLException $exception
     * @return EventId|null - sentry event id or null
     */
    public function generateLogFromException(GraphQLException $exception): EventId|null {
        return $this->generateLog($exception);
    }

    /**
     * @param DebugObject $debugObject
     * @return EventId|null - sentry event id or null
     */
    public function generateLogFromDebugObject(DebugObject $debugObject): EventId|NULL {

        if (!$this->logDebugMessages) { return null; }
        return $this->generateLog(null, $debugObject);
    }

    /**
     * Generate the curl log
     * @todo don't show request in test cases
     * @param GraphQLException|null $exception
     * @param DebugObject|null $debugObjectParam
     * @return EventId|null - sentry event id or null
     */
    private function generateLog(?GraphQLException $exception = null, ?DebugObject $debugObjectParam = null): EventId|null {

        $this->setUpLogDir();
        $exceptionMessage = $exception === null ? '' : $exception->getMessage();
        $exceptionCode = $exception === null ? '' : $exception->getCode();
        $timeOfLogEntry = date('d/m/Y H:i:s');

        $debugObject = $exception === null ? $debugObjectParam : $exception->debugObject;
        if ($debugObject === null) { return null; }
        $debugObjectParameters = $debugObject->parameters;
        $debugObjectRequest = $debugObject->request;
        $this->version = $this->version === '0' ? 'which is not set' : $this->version;

        $currentUrl = $debugObjectRequest !== null ? $debugObjectRequest->getUrl() : 'Current client location not available.';
        $currentIp = $debugObjectRequest !== null ? CommunicationUtil::getClientIp($debugObjectRequest) : 'none';
        $currentCountry = $debugObjectRequest !== null ? CommunicationUtil::getClientCountry($debugObjectRequest) : 'unknown';
        $clientId = $debugObject->userDebug->userId !== null ? $debugObject->userDebug->userId : 'anonymous';
        $identityJson = $responseJson = $parametersJson = $cacheJson = $communicatorJson = $parsedErrors = $requestHeaders = $requestBody = null;
        $vuek = null;

        try {
            $identityUser = $debugObject->communicatorDebug->userId
                ? json_encode(['User ID' => $debugObject->communicatorDebug->userId, 'Token' => $debugObject->communicatorDebug->bearerToken], JSON_THROW_ON_ERROR)
                : 'Without identity';

            $vuek = $debugObject->virtualUniqErrorKeys === null ? 'Without virtual uniq keys' : json_encode($debugObject->virtualUniqErrorKeys);
            $identityJson = $debugObject->userDebug->userData === null ? $identityUser : json_encode($debugObject->userDebug->userData, JSON_THROW_ON_ERROR);
            $responseJson = json_encode($debugObject->requestDebug, JSON_THROW_ON_ERROR);
            $parametersJson = json_encode($debugObjectParameters, JSON_THROW_ON_ERROR);
            $cacheJson = json_encode($debugObject->cacheDebug, JSON_THROW_ON_ERROR);
            $communicatorJson = json_encode($debugObject->communicatorDebug, JSON_THROW_ON_ERROR);
            $parsedErrors = $debugObject->errors === null ? 'Without parsed errors' : json_encode($debugObject->errors, JSON_THROW_ON_ERROR);
            $requestHeaders = $debugObjectRequest !== null ? json_encode($debugObjectRequest->getHeaders(), JSON_THROW_ON_ERROR) : 'Headers not available.';
            $requestBody = $debugObjectRequest?->getRawBody();
            $requestBody = $requestBody === null || $requestBody === '' ? 'Body not available.' : json_encode($debugObjectRequest->getRawBody(), JSON_THROW_ON_ERROR);
        } catch (JsonException) {}

        // Init local
        $msgDebug = "Time of log entry: {$timeOfLogEntry}                                                        \n\n";
        $msgDebug .= "Exception: [{$exceptionCode}] {$exceptionMessage}                                          \n\n";
        $msgDebug .= "Canonized identify: {$debugObject->canonizedIdentify}                                      \n\n";
        if ($debugObjectParameters instanceof SendGraphQLRequestParameters) {
            $msgDebug .= "GraphQL query \n {$debugObjectParameters->query}                                       \n\n";
            $msgDebug .= "VUEK \n {$vuek}                                                                        \n\n";
        } else {
            $msgDebug .= "Endpoint {$debugObjectParameters->endpoint}                                            \n\n";
        }

        $msgDebug .= "API response \n {$debugObject->requestDebug->responseBody}                                 \n\n";
        $msgDebug .= "Identity debug \n {$identityJson}                                                          \n\n";
        $env = $this->environment ?? 'N/A';

        if (!$this->localLogSimplified) {
            $msgDebug .= "Environment: {$env} in version {$this->version}                                        \n\n";
            $msgDebug .= "Current URL:  {$currentUrl}                                                            \n\n";
            $msgDebug .= "Visitor info: CID {$clientId} IP {$currentIp} FROM {$currentCountry}.                  \n\n";
            $msgDebug .= "Input parameters \n  {$parametersJson}                                                 \n\n";
            $msgDebug .= "Parsed errors \n  {$parsedErrors}                                                      \n\n";
            $msgDebug .= "Communication debug \n  {$responseJson}                                                \n\n";
            $msgDebug .= "Redis cache debug \n  {$cacheJson}                                                     \n\n";
            $msgDebug .= "Library debug \n  {$communicatorJson}                                                  \n\n";
            $msgDebug .= "Request headers \n  {$requestHeaders}                                                  \n\n";
            $msgDebug .= "Request body \n  {$requestBody}                                                        \n\n";
        }

        if ($exception !== null) {
            $this->saveExceptionToLocalStorage($msgDebug);
        } else {
            $this->saveDebugObjectToLocalStorage($msgDebug);
        }

        if ($exception !== null && ($this->exceptionIsIgnored($exception) || $this->isExceptionSpike($exception))) {
            return null;
        }
        if ($exception === null) {
            return null;
        }

        return $this->sendExceptionToSentry($debugObject, $exception);
    }

    /**
     * Save uniq exception into cache
     * If already saved skip it
     * @param GraphQLException $exception
     * @return bool - found spike
     */
    private function isExceptionSpike(GraphQLException $exception): bool {

        if ($this->spikeTimeoutSeconds === null) { return false; }
        if ($exception->debugObject->parameters instanceof SendGraphQLRequestParameters) {
            $identify = $exception->debugObject->parameters->query;
        } else {
            $identify = $exception->debugObject->parameters->endpoint;
        }

        $message = $exception->getMessage();
        $code = $exception->getCode();
        $uniqCacheKey = __DIR__ . "-{$identify}-{$exception}-{$code}-{$message}";
        $exceptionData = $this->cache->read($uniqCacheKey);
        if ($exceptionData !== null) { return true; }
        $this->cache->write($uniqCacheKey, 's', [Cache::Expire => $this->spikeTimeoutSeconds, Cache::Tags => self::spikeProtectionCacheTag]);
        return false;
    }

    private function exceptionIsIgnored(GraphQLException $exception): bool {

        foreach ($this->ignored as $patternGroupName => $patternGroup) {

            $patternGroupIgnoredCount = 0;
            foreach ($patternGroup as $pattern) {

                // Query name identification
                if (str_contains($exception->debugObject->canonizedIdentify ?? '', $pattern)) {
                    ++$patternGroupIgnoredCount;
                    continue;
                }

                // VUEK identification
                if ($exception->virtualUniqErrorKeys !== null &&
                    count(array_filter($exception->virtualUniqErrorKeys, function(string $virtualUniqErrorKey) use ($pattern) {
                        if (!is_string($pattern)) { return false; }
                        return str_contains($virtualUniqErrorKey, $pattern);
                    })) > 0) {
                    ++$patternGroupIgnoredCount;
                    continue;
                }

                // Timeout identification
                if ($pattern === ':TIMEOUT' && $exception instanceof GraphQLApiInternalException && $exception->timeoutErrorPresented()) {
                    ++$patternGroupIgnoredCount;
                    continue;
                }

                // Handle ignoring specific HTTP codes
                preg_match_all('/:([0-9]{3})/m', $pattern, $matches);
                $patternHttpCode = isset($matches[1][0]) ? intval($matches[1][0]) : null;
                if ($patternHttpCode !== null && $exception->debugObject->requestDebug->responseHttpCode === $patternHttpCode) {
                    ++$patternGroupIgnoredCount;
                    continue;
                }
            }

            if ($patternGroupIgnoredCount >= count($patternGroup)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Log exception into log directory
     * @param string $messageData
     */
    private function saveExceptionToLocalStorage(string $messageData): void {

        $message = $messageData . self::Line . self::Line . "\n\n";
        $message = str_replace(['|', '`'], ["\n", ''], $message);
        $fileName = new DateTime()->format('Y-m-d') . '.txt';
        file_put_contents("{$this->logPath}/exceptions/{$fileName}", $message, FILE_APPEND);
    }

    /**
     * Log debug object into log directory
     * @param string $messageData
     */
    private function saveDebugObjectToLocalStorage(string $messageData): void {

        $message = $messageData . self::Line . self::Line . "\n\n";
        $message = str_replace(['|', '`'], ["\n", ''], $message);
        $fileName = new DateTime()->format('Y-m-d') . '.txt';
        file_put_contents("{$this->logPath}/debug/{$fileName}", $message, FILE_APPEND);
    }

    /**
     * Send exception into sentry
     * @param DebugObject $debugObject
     * @param GraphQLException $exception
     * @return EventId|null - sentry event id or null
     */
    private function sendExceptionToSentry(DebugObject $debugObject, GraphQLException $exception): EventId|null {

        return withScope(function (Scope $scope) use ($debugObject, $exception) {
            Utils::updateScopeFromDebugObject($scope, $debugObject);
            return captureException($exception);
        });
    }
}