<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace GraphQLFrontApi\Communication;

use GraphQLFrontApi\Communication\Entities\RequestTypes;
use GraphQLFrontApi\Communication\Entities\SendGraphQLRequestParameters;
use GraphQLFrontApi\Entities\GuzzleHandle;
use GraphQLFrontApi\Exceptions\Communication\GraphQLApiException;
use GraphQLFrontApi\Exceptions\Communication\GraphQLApiInternalException;
use GraphQLFrontApi\Exceptions\Communication\GraphQLApiNotFoundException;
use GraphQLFrontApi\Exceptions\Communication\GraphQLApiUnauthorizedException;
use GraphQLFrontApi\Exceptions\GraphQLCodeException;
use GraphQLFrontApi\Utils;
use JsonException;
use Nette\Application\AbortException;
use Nette\Caching\Cache;

/**
 * Class GraphQLCommunication
 * @package GraphQLFrontApi
 */
class GraphQLCommunication extends Communication {

    private SendGraphQLRequestParameters $specificParameters;
    public function setGraphQLDomain(string $graphQLDomain): void { $this->graphQLDomain = $graphQLDomain; }
    public function setPerformanceLoggingEnable(bool $performanceLoggingEnable): void { $this->performanceLoggingEnable = $performanceLoggingEnable; }
    public function setPerformanceLoggingSampleRate(float $performanceLoggingSampleRate): void { $this->performanceLoggingSampleRate = $performanceLoggingSampleRate; }
    public function setPerformanceLoggingFile(string $performanceLoggingFile): void { $this->performanceLoggingFile = $performanceLoggingFile; }
    public function setDisableCache(bool $disableCache): void { $this->disableCache = $disableCache; }

    /**
     * Send the request to the API
     * @param SendGraphQLRequestParameters $parameters
     * @return array
     * @throws GraphQLApiException
     * @throws GraphQLApiInternalException
     * @throws GraphQLApiNotFoundException
     * @throws GraphQLApiUnauthorizedException
     * @throws GraphQLCodeException
     */
    public function sendRequestGraphQuery(SendGraphQLRequestParameters $parameters): array {

        $this->requestParameters = $parameters;
        $this->specificParameters = $parameters;
        $this->initializeDebugObject($this->logCallStack);
        $this->checkPreConditions();
        $this->initializePropertiesFromPresenter();
        $cachedData = $this->initCacheAndTryToLoadResult();

        if ($cachedData !== null && !$this->freshUpdate) {
            $this->debugGraphQL();
            return $cachedData;
        }

        $this->initializeGuzzleRequest();
        $this->addPostFieldsToRequest();
        $this->addAuthFieldsToRequest();
        $this->addLangFieldsToRequest();
        $this->addRemoteInfoFieldsToRequest();
        $this->sendGraphQLRequest();
        $this->updateCache();
        $this->processErrors();
        $this->debugGraphQL();

        return $this->result['data'] ?? throw new GraphQLCodeException(message: 'Impossible to find data output in result!', debugObject: $this->debugObject);
    }

    /** Append GraphQL request parameters */
    protected function initializeDebugObject(bool $logCallStack): void {

        parent::initializeDebugObject($logCallStack);
        $this->debugObject->canonizedIdentify = Utils::getSentryPerformanceIdentify($this->specificParameters->query);
        $this->debugObject->parameters = $this->specificParameters;
        $this->debugObject->communicatorDebug->domain = $this->graphQLDomain;
    }

    /**
     * Initialize cache key and try to load data from cache
     * Save cache key into property and return array with data or null
     * @return array|null
     * @throws GraphQLCodeException
     */
    protected function initCacheAndTryToLoadResult(): ?array {

        // Create key
        $this->cacheKey = null;
        $userKey = $this->userId ?? 'user';
        $languageKey = $this->languageISO ?? 'language';
        $queryKey = hash('md5', $this->specificParameters->query);
        $locationKey = __DIR__;
        $typeKey = $this->requestParameters->type->getValue();

        // Remove specific variable parts of key
        if ($this->specificParameters->disableCachePerUser === true) { $userKey = 'user'; }
        if ($this->specificParameters->disableCachePerLanguage === true) { $languageKey = 'language'; }

        // Save cache key
        $this->cacheKey = "{$locationKey}-{$typeKey}-{$userKey}-{$languageKey}-{$queryKey}";
        $this->debugObject->cacheDebug->cachedKey = $this->cacheKey;

        // Handle disabled cache
        if ($this->disableCache) { return null; }
        if ($this->specificParameters->enableCacheRead !== true) { return null; }

        // Load from cache
        $storedData = $this->cache->read($this->cacheKey);
        $this->debugObject->cacheDebug->result = $storedData;
        if ($storedData === null) { return null; }
        try { $decoded = json_decode($storedData, true, 512, JSON_THROW_ON_ERROR); } catch (JsonException $e) {
            throw new GraphQLCodeException(message: 'Impossible to decode cached entity!', debugObject: $this->debugObject, previous: $e);
        }

        // Parse data
        if (!isset($decoded['data'])) { throw new GraphQLCodeException(message: 'Impossible to return data from cached data!', debugObject: $this->debugObject); }
        $this->debugObject->cacheDebug->cached = true;
        return $decoded['data'];
    }

    /** Debug GraphQL request */
    protected function debugGraphQL(): void { $this->debug($this->specificParameters->query); }

    /** Create new Guzzle instance with API URI */
    function initializeGuzzleRequest(): void { $this->guzzleHandle = new GuzzleHandle(uri: $this->graphQLDomain); }

    /**
     * Initialize Guzzle configuration for this request
     * @throws GraphQLCodeException
     */
    function addPostFieldsToRequest(): void {

        // Init headers
        $this->guzzleHandle->requestType = $this->specificParameters->type;
        $this->guzzleHandle->headers['Content-Type'] = $this->specificParameters->contentType;
        if ($this->specificParameters->type->getValue() === RequestTypes::GET) { return; }

        // If POST encode query
        try {
            $this->guzzleHandle->requestBody = json_encode(['query' => $this->specificParameters->query], JSON_THROW_ON_ERROR);
            $this->guzzleHandle->headers['Content-Length'] = strlen($this->guzzleHandle->requestBody);
        } catch (JsonException $e) {
            throw new GraphQLCodeException(message: 'Impossible to encode query!', debugObject: $this->debugObject, previous: $e);
        }
    }

    /**
     * Send GraphQL request
     * @throws AbortException
     * @throws GraphQLApiInternalException
     * @throws GraphQLApiUnauthorizedException
     * @throws GraphQLCodeException
     * @throws GraphQLApiNotFoundException
     */
    protected function sendGraphQLRequest(): void {
        $this->sendRequest($this->specificParameters->query, Utils::getSentryPerformanceIdentify($this->specificParameters->query));
    }

    /**
     * Update cache
     * Delete requested tags and create new entry with key and tags
     */
    protected function updateCache(): void {

        // Cache disabled globally
        if ($this->disableCache) { return; }

        // Delete cached tags
        if ($this->specificParameters->invalidateTags !== null) {
            $this->cache->clean([Cache::Tags => $this->specificParameters->invalidateTags]);
        }

        // Get calling function for cache tag
        $callerFunction = self::generateDeletableCacheKeyForEndpoint(
            canonizedQuery: Utils::getSentryPerformanceIdentify($this->specificParameters->query),
            user: $this->userId,
            language: $this->languageISO
        );

        // Tags
        $saveTags = [$callerFunction];
        if ($this->userId !== null) { $saveTags[] = $this->userId; }
        if ($this->specificParameters->additionalCacheTags !== null) { $saveTags = array_merge($saveTags, $this->specificParameters->additionalCacheTags); }

        // Write or skip if requested
        $this->debugObject->cacheDebug->tags = $saveTags;
        if ($this->responseBodyContent !== null && $this->specificParameters->validSeconds !== null) {
            $this->cache->write($this->cacheKey, $this->responseBodyContent, [Cache::Expire => $this->specificParameters->validSeconds, Cache::Tags => $saveTags]);
        }
    }

    /**
     * GraphQL base URL
     * @return string|null
     */
    function getApiAddress(): string|null { return $this->graphQLDomain; }

    /**
     * Create additional cache tag to be used to delete data from cache
     * @param string $canonizedQuery - canonized endpoint name (f.e. ticketList)
     * @param string|null $user - user id
     * @param string|null $language - language iso code
     * @return string - cache key for data remove
     */
    public static function generateDeletableCacheKeyForEndpoint(string $canonizedQuery, string|null $user, string|null $language): string {

        $locationKey = __DIR__;
        $typeKey = RequestTypes::POST;
        $userKey = $user ?? 'user';
        $languageKey = $language ?? 'language';
        return "{$locationKey}-{$typeKey}-{$userKey}-{$languageKey}-{$canonizedQuery}";
    }
}
