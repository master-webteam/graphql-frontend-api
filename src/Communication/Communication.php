<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace GraphQLFrontApi\Communication;

use GraphQLFrontApi\BaseNettePresenter;
use GraphQLFrontApi\Communication\Entities\SendRequestParameters;
use GraphQLFrontApi\Communication\FrontHandlers\AFrontHandler;
use GraphQLFrontApi\Communication\FrontHandlers\InternalFrontHandlerWrapper;
use GraphQLFrontApi\Debug\DebugObject;
use GraphQLFrontApi\Entities\GuzzleHandle;
use GraphQLFrontApi\Exceptions\Communication\GraphQLApiException;
use GraphQLFrontApi\Exceptions\Communication\GraphQLApiInternalException;
use GraphQLFrontApi\Exceptions\Communication\GraphQLApiNotFoundException;
use GraphQLFrontApi\Exceptions\Communication\GraphQLApiUnauthorizedException;
use GraphQLFrontApi\Exceptions\GraphQLCodeException;
use GraphQLFrontApi\Logger;
use GraphQLFrontApi\Utils;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\TransferStats;
use JsonException;
use Nette\Application\AbortException;
use Nette\Caching\Storage;
use Nette\Http\Request;
use Psr\Http\Message\ResponseInterface;
use function Sentry\configureScope;
use function Sentry\startTransaction;
use Sentry\State\Scope;
use Sentry\Tracing\SpanContext;
use Sentry\Tracing\TransactionContext;
use Tracy\Debugger;

abstract class Communication {

    protected const string PARSE_FUNCTION_REGEX = '/([a-zA-Z_\-]+)\(.+\)/m';

    // Contains base GraphQL endpoint URL [DI]
    protected string $graphQLDomain;

    // Contains file API endpoint URL [DI]
    protected ?string $filesDomain = null;

    // If enabled logging is set and cache is disabled [DI]
    protected bool $debugModeEnabled;

    // Tell if API don't use any auth [DI]
    protected bool $unauthorizedMode;

    // Where to redirect on unauthorized [DI]
    protected string $unauthorizedLink;

    // Where to redirect on token expire [DI]
    protected string $loginLink;

    // Regex terms which should be skipped from reporting as error [DI]
    public ?array $ignoredErrorRegexes = null;

    // If this flag is enabled - the cache is disabled [PUBLIC]
    public bool $freshUpdate = false;

    // Contains presenter [PUBLIC]
    public ?BaseNettePresenter $presenterObject = null;

    // If used by test case or auth [PUBLIC]
    public bool $presenterUnattached = false;
    public bool $feHandlerUnattached = false;

    // Contains request parameters [PARAM]
    protected SendRequestParameters $requestParameters;

    // Contains User ID from presenter [INIT/PUBLIC]
    public ?string $userId = null;

    // Contains Bearer Token [INIT/PUBLIC]
    public ?string $bearerToken = null;

    // Contains current locale [INIT]
    protected ?string $languageISO = null;

    // Current debug object [INIT]
    protected DebugObject $debugObject;

    // Current curl object [INIT]
    protected GuzzleHandle $guzzleHandle;

    // Current cache key [INIT]
    protected ?string $cacheKey = null;

    // Guzzle response body content [INIT]
    protected string|null $responseBodyContent = null;

    /**
     * Guzzle response headers
     * @var string[][]|null
     */
    protected array|null $responseHeaders = null;

    // Output result [INIT]
    protected ?array $result = null;

    // Disable cache on requests [DI]
    protected bool $disableCache = false;

    // If performance logging is enabled [DI]
    protected bool $performanceLoggingEnable;

    // Percents in float of requests to be logged [DI]
    protected float $performanceLoggingSampleRate;

    // File where will be performance logging saved [DI]
    protected string $performanceLoggingFile;

    // Contains custom exception mapping [key:objectClass]=ExceptionClassFE [CONFIG]
    private ?array $customApiExceptionsMapping = null;

    // Frontend handler adapter object [SETTER]
    private ?InternalFrontHandlerWrapper $internalFrontHandler = null;

    // Contains custom exception mapping [key:uniqCode]=TranslationKey [CONFIG]
    private ?array $customAutoResponsePayloadUniqCodeMapping = null;

    // Contains custom translation strings mapping for special events [key:eventName]=TranslationKey [CONFIG]
    private ?array $customTranslationKeys = null;

    // Log call stack for every call [DI]
    protected bool $logCallStack;

    public function __construct(
        protected readonly Logger $logger,
        protected readonly Storage $cache,
        protected readonly ?Request $request = null) {
    }

    public function setLogCallStack(bool $logCallStack): void { $this->logCallStack = $logCallStack; }
    public function setDebug(bool $debugMode): void { $this->debugModeEnabled = $debugMode; }
    public function setUnauthorizedMode(bool $unauthorizedMode): void { $this->unauthorizedMode = $unauthorizedMode; }
    public function setUnauthorizedLink(string $unauthorizedLink): void { $this->unauthorizedLink = $unauthorizedLink; }
    public function setLoginLink(string $loginLink): void { $this->loginLink = $loginLink; }

    /**
     * Initialize service for tests
     * @param string|null $userId
     * @param string|null $token
     */
    public function setScriptMode(?string $userId = null, ?string $token = null): void {

        $this->presenterUnattached = true;
        $this->feHandlerUnattached = true;
        $this->freshUpdate = true;
        if ($userId !== null) { $this->userId = $userId; }
        if ($token !== null) { $this->bearerToken = $token; }
    }

    /**
     * Initialize service for tests
     * @param string|null $userId
     * @param string|null $token
     */
    public function setTestMode(?string $userId = null, ?string $token = null): void {

        $this->setScriptMode($userId, $token);
        $this->disableCache = true;
    }

    /**
     * Initialize custom mapping for custom exceptions
     * @param array|null $mappings - config mappings
     */
    public function setCustomApiExceptionsMapping(array|null $mappings): void {

        if ($mappings === null) { return; }
        $this->customApiExceptionsMapping = [];
        foreach ($mappings as $mappingExceptionName => $mappingObjectParams) {
            $this->customApiExceptionsMapping[$mappingExceptionName] = $mappingObjectParams['objectClass'];
        }
    }

    /**
     * Initialize custom mapping for uniq error codes
     * @param array|null $mappings - config mappings
     */
    public function setAutoResponsePayloadUniqCodeMapping(array|null $mappings): void {

        if ($mappings === null) { return; }
        $this->customAutoResponsePayloadUniqCodeMapping = [];
        foreach ($mappings as $mappingUniqCode => $mappingObjectParams) {
            $this->customAutoResponsePayloadUniqCodeMapping[$mappingUniqCode] = $mappingObjectParams['translationKey'];
        }
    }

    /**
     * Initialize mapping of translation keys for special events
     * @param array|null $mappings - config mappings
     */
    public function setCustomTranslationKeys(array|null $mappings): void {

        if ($mappings === null) { return; }
        $this->customTranslationKeys = [];
        foreach ($mappings as $eventName => $translationKey) {
            $this->customTranslationKeys[$eventName] = $translationKey;
        }
    }

    /**
     * Set frontend handler
     * @param AFrontHandler $frontHandler
     */
    public function setFrontHandler(AFrontHandler $frontHandler): void {
        $this->internalFrontHandler = new InternalFrontHandlerWrapper(
            $this->customTranslationKeys, $this->customAutoResponsePayloadUniqCodeMapping, $frontHandler);
    }

    /**
     * Create new Guzzle instance
     * @throws GraphQLCodeException
     */
    abstract function initializeGuzzleRequest(): void;

    /**
     * Initialize output data
     * @throws GraphQLCodeException
     */
    abstract function addPostFieldsToRequest(): void;

    /**
     * Get API URL address
     * @return string|null
     */
    abstract function getApiAddress(): string|null;
    /**
     * Check input conditions
     * @throws GraphQLCodeException
     */
    protected function checkPreConditions(): void {

        if ($this->feHandlerUnattached) {
            $this->requestParameters->useBearerTokenFromPresenter = false;
        }
        if ($this->internalFrontHandler === null && !$this->feHandlerUnattached) {
            throw new GraphQLCodeException(message: 'Presenter unattached. Fix or use presenterUnattached param!', debugObject: $this->debugObject);
        }
        if (!$this->requestParameters->useBearerTokenFromPresenter && $this->requestParameters->bearerToken === null && $this->bearerToken === null && !$this->unauthorizedMode) {
            throw new GraphQLCodeException(message: 'Authorized mode without any bearer token!', debugObject: $this->debugObject);
        }
    }

    /**
     * Initialize User ID and User Bearer Token
     * Check if user is logged and try to get base info
     */
    protected function initializePropertiesFromPresenter(): void {

        // If we don't have presenter try to look for user and auth in params
        $this->userId = $this->userId ?? $this->requestParameters->forceUserId;
        $this->bearerToken = $this->bearerToken ?? $this->requestParameters->bearerToken;
        if ($this->feHandlerUnattached || $this->internalFrontHandler === null) { return; }

        // Initialize FE info for user
        $this->languageISO = $this->internalFrontHandler->frontHandler->getLocale();
        $this->userId = $this->userId ?? $this->internalFrontHandler->frontHandler->getUserId();
        $this->bearerToken = $this->bearerToken ?? $this->internalFrontHandler->frontHandler->getUserToken();
        $this->debugObject->userDebug->userData = $this->internalFrontHandler->frontHandler->getUserData();
        $this->debugObject->userDebug->userId = $this->userId;
        $this->debugObject->userDebug->userToken = $this->bearerToken;
    }

    /**
     * Initialize debug object
     * Fill basic class variables into some debug object wrapper
     */
    protected function initializeDebugObject(bool $logCallStack): void {

        $debugObject = new DebugObject($this->internalFrontHandler);
        $debugObject->logger = $this->logger;
        $debugObject->request = $this->request;
        $debugObject->communicatorDebug->userId = $this->userId;
        $debugObject->communicatorDebug->debugModeEnabled = $this->debugModeEnabled;
        $debugObject->communicatorDebug->freshUpdate = $this->freshUpdate;
        $debugObject->communicatorDebug->languageISO = $this->languageISO;
        $debugObject->communicatorDebug->unauthorizedMode = $this->unauthorizedMode;
        $debugObject->stack = $logCallStack ? debug_backtrace() : null;
        $this->debugObject = $debugObject;
    }

    /**
     * Initialize authorization part of request
     * If requested append Bearer token into request
     */
    protected function addAuthFieldsToRequest(): void {

        $this->debugObject->communicatorDebug->bearerToken = $this->bearerToken;
        if ($this->unauthorizedMode) { return; }
        $bearerToken = $this->bearerToken;
        if ($bearerToken === null) { return; }
        $this->guzzleHandle->headers['Authorization'] = 'Bearer ' . $bearerToken;
    }

    /**
     * Initialize language header
     * If requested send accept language header
     */
    protected function addLangFieldsToRequest(): void {

        if ($this->requestParameters->dontSendLanguageInRequest) { return; }
        $language = $this->requestParameters->languageISO ?? $this->languageISO;
        $this->guzzleHandle->headers['Accept-Language'] = $language;
    }

    /**
     * Initialize request info into headers
     * Look into request and send info in headers
     * @throws GraphQLCodeException
     */
    protected function addRemoteInfoFieldsToRequest(): void {

        if ($this->requestParameters->dontSendRequestInfoInRequest) {
            return;
        }
        if ($this->request === null) {
            return;
        }

        $parameters = is_array($this->request->getQuery()) ? $this->request->getQuery() : null;
        try {
            $parameters = $parameters !== null ? json_encode($parameters, JSON_THROW_ON_ERROR) : null;
        } catch (JsonException $e) {
            throw new GraphQLCodeException(message: 'Impossible to encode request parameters!', debugObject: $this->debugObject, previous: $e);
        }

        $url = $this->request->getUrl()->getAbsoluteUrl() === 'http:/' ? null : $this->request->getUrl()->getAbsoluteUrl();
        $this->guzzleHandle->headers['CIS-Connecting-IP'] = CommunicationUtil::getClientIp($this->request);
        $this->guzzleHandle->headers['CIS-Connecting-Country'] = CommunicationUtil::getClientCountry($this->request);
        $this->guzzleHandle->headers['CIS-Connecting-Agent'] = $this->request->getHeader('User-Agent') ?? 'Unknown';
        $this->guzzleHandle->headers['CIS-Connecting-Host'] = $this->request->getRemoteHost() ?? 'Unknown';
        $this->guzzleHandle->headers['CIS-Connecting-Language'] = $this->request->getHeader('Accept-language') ?? 'Unknown';
        $this->guzzleHandle->headers['CIS-Connecting-Async'] = $this->request->isAjax() ? 'Yes' : 'No';
        $this->guzzleHandle->headers['CIS-Connecting-URL'] = $url ?? 'Unknown';
        $this->guzzleHandle->headers['CIS-Connecting-Parameters'] = $parameters ?? 'Unknown';
    }

    /**
     * Call request and get response
     * @throws AbortException
     * @throws GraphQLCodeException
     * @throws GraphQLApiInternalException
     * @throws GraphQLApiUnauthorizedException
     * @throws GraphQLApiNotFoundException
     */
    protected function sendRequest(string $identify, string $canonizedIdentify): void {

        // Initialize request metrics
        $this->debugObject->canonizedIdentify = $canonizedIdentify;
        $this->debugObject->requestDebug->guzzleHandle = $this->guzzleHandle;
        list($transaction, $span1) = $this->prepareForRun(
            identify: $identify,
            canonizedIdentify: $canonizedIdentify
        );

        // Run request to API
        $res = null; $errno = null;
        try {
            $res = new Client()->send(
                request: new \GuzzleHttp\Psr7\Request(
                    method: $this->guzzleHandle->requestType->getValue(),
                    uri: $this->guzzleHandle->uri,
                    headers: $this->guzzleHandle->headers,
                    body: $this->guzzleHandle->requestBody
                ),
                options: [
                    'http_errors' => false, // Don't throw exceptions on 4xx 5xx HTTP codes
                    'connect_timeout' => $this->requestParameters->curlTimeout,
                    'timeout' => $this->requestParameters->curlTimeout,
                    'synchronous' => true,
                    'multipart' => $this->guzzleHandle->multipartOptions,
                    'on_stats' => function (TransferStats $stats) {
                        $this->debugObject->requestDebug->transferTime = $stats->getTransferTime();
                        $this->debugObject->requestDebug->handlerStats = $stats->getHandlerStats();
                    },
                    'on_headers' => function (ResponseInterface $response) {
                        $this->responseHeaders = $response->getHeaders();
                        $this->debugObject->requestDebug->responseHeaders = $this->responseHeaders;
                    }
                ]
            );
        } catch (GuzzleException $e) { $errno = $e->getMessage(); }

        // Save metrics
        $this->finishRun(
            sentrySpan: $span1,
            sentryTransaction: $transaction,
        );

        // Process result
        $this->processCurlResult(
            canonizedIdentify: $canonizedIdentify,
            identify: $identify,
            errnoMsg: $errno,
            response: $res,
        );
    }

    /**
     * Initialize debug object before run
     * Configure Sentry
     * @param string $identify
     * @param string $canonizedIdentify
     * @return array
     */
    protected function prepareForRun(string $identify, string $canonizedIdentify): array {

        // Init sentry scope
        configureScope(function (Scope $scope): void {
            Utils::updateScopeFromDebugObject($scope, $this->debugObject);
        });

        // Sentry performance monitoring
        $transactionContext = new TransactionContext();
        $transactionContext->setName($canonizedIdentify);
        $transactionContext->setOp('http.caller');
        $transaction = startTransaction($transactionContext);
        $spanContext = new SpanContext();
        $spanContext->setDescription($identify);
        $spanContext->setOp('GraphQL Query');
        $span1 = $transaction->startChild($spanContext);
        return [$transaction, $span1];
    }

    /**
     * Process CURL result and trigger actions
     * @param string $canonizedIdentify
     * @param string $identify
     * @param string|null $errnoMsg
     * @param ResponseInterface|null $response
     * @throws GraphQLApiInternalException
     * @throws GraphQLApiNotFoundException
     * @throws GraphQLApiUnauthorizedException
     * @throws GraphQLCodeException
     */
    protected function processCurlResult(string $canonizedIdentify, string $identify, string|null $errnoMsg, ResponseInterface|null $response): void {

        $responseStatusCode = $response?->getStatusCode();
        $responseStatusCodeMessage = $responseStatusCode === null ? null : Utils::httpCodeToMessage($responseStatusCode);
        $responseBodyContent = $response?->getBody()->getContents();

        $this->debugObject->requestDebug->errnoMsg = $errnoMsg;
        $this->debugObject->requestDebug->responseBody = $responseBodyContent;
        $this->debugObject->requestDebug->responseHttpCode = $responseStatusCode;
        $this->debugObject->requestDebug->responseHttpCodeMessage = $responseStatusCodeMessage;

        // Handle some status codes with exceptions
        $expStatusCode = $responseStatusCodeMessage === null ? '' : " - {$responseStatusCodeMessage}";
        $exp = match ($responseStatusCode) {
            403, 401, 200 => null, // Process later
            404 => new GraphQLApiNotFoundException(debugObject: $this->debugObject, message: "[CURL] {$canonizedIdentify} - Backend communication not found error!"),
            default => new GraphQLApiInternalException(debugObject: $this->debugObject, message: $errnoMsg !== null
                ? "[CURL] {$canonizedIdentify} - {$expStatusCode} Error: {$errnoMsg}!"
                : "[CURL] {$canonizedIdentify} - {$expStatusCode} Error!", code: $responseStatusCode),
        };

        // If is already exception return it
        if ($exp !== null) { throw $exp; }

        // Correct payload is processed
        $this->responseBodyContent = $responseBodyContent;
        if ($responseStatusCode === 200) {
            $this->processResponseData();
            return;
        }

        // Unauthorized result handler
        if (in_array($responseStatusCode, [403, 401], true)) {
            if ($this->requestParameters->dontProcessUnauthorizedException === true || $this->feHandlerUnattached === true) {
                throw new GraphQLApiUnauthorizedException($this->debugObject, "[CURL] {$canonizedIdentify} - Backend communication unauthorized error!");
            }
            $this->debug($identify);
            if ($responseStatusCode === 401) {
                $this->internalFrontHandler->frontHandler->handleLogout($this->loginLink);
            } else {
                $this->internalFrontHandler->frontHandler->handleRedirect($this->unauthorizedLink);
            }
        }
    }

    /**
     * Process string response from cURL and parse it
     * @throws GraphQLCodeException
     */
    protected function processResponseData(): void {

        // If requested, decode output data into array
        if ($this->requestParameters->dontProcessResultData) { return; }
        try { $resultDecoded = json_decode($this->responseBodyContent, true, 512, JSON_THROW_ON_ERROR); } catch (JsonException $e) {
            throw new GraphQLCodeException(message: 'Error decoding API result', debugObject: $this->debugObject, previous: $e);
        }

        // Produce parsed output
        $this->debugObject->requestDebug->resultArray = $resultDecoded;
        $this->result = $resultDecoded;
    }

    /**
     * Process errors
     * @throws GraphQLApiUnauthorizedException
     * @throws GraphQLApiException
     */
    protected function processErrors(): void {

        // Search for errors
        if (!isset($this->result['errors']) && !isset($this->result['error'])) {
            return;
        }

        // Unauthorized simple
        if (isset($this->result['error']) && $this->result['error'] === 'Unauthorized' && !$this->requestParameters->dontProcessUnauthorizedException) {
            throw new GraphQLApiUnauthorizedException(debugObject: $this->debugObject);
        }

        // Unauthorized multi
        if (isset($this->result['errors']) && !$this->requestParameters->dontProcessUnauthorizedException) {
            foreach ($this->result['errors'] as $error) {

                if (isset($error['extensions']['objectClass'])
                    && $this->customApiExceptionsMapping !== null
                    && isset($this->customApiExceptionsMapping[$error['extensions']['objectClass']])
                    && class_exists($this->customApiExceptionsMapping[$error['extensions']['objectClass']])) {
                    throw new $this->customApiExceptionsMapping[$error['extensions']['objectClass']]($this->debugObject);
                }
                if (isset($error['extensions']['errorType']) && $error['extensions']['errorType'] === 'AUTHENTICATION_ERR') {
                    throw new GraphQLApiUnauthorizedException(debugObject: $this->debugObject);
                }
            }
        }

        // Warning
        throw new GraphQLApiException($this->debugObject);
    }

    /**
     * Debug on script finish
     * We want to show in bar
     */
    protected function debug(string $identify): void {

        $this->logger->generateLogFromDebugObject($this->debugObject);
        if (!$this->debugModeEnabled) {
            return;
        }
        Debugger::barDump($this->debugObject, $identify);
    }

    /**
     * Finish CURL instance run
     * @param mixed $sentrySpan
     * @param mixed $sentryTransaction
     */
    protected function finishRun(mixed $sentrySpan, mixed $sentryTransaction): void {

        // Finish run
        $sentrySpan->finish();
        $sentryTransaction->finish();
    }
}
