<?php

declare(strict_types=1);

/** @noinspection PhpUnused */

namespace GraphQLFrontApi\Communication\FrontHandlers;

use Sentry\EventId;

class ResponsePayload {

    /**
     * @generated
     * @var string[]|null
     */
    public ?array $uniqKeys = null;

    /**
     * @generated
     * @var string|null
     */
    public ?string $eventId = null;

    /**
     * @param string $message - response message
     * @param ResponsePayloadTypeEnum $type - type of message
     * @param string[]|int[]|null[]|null $uniqKeys - uniq keys to identify problem
     * @param EventId|null $sentryEventId - sentry event id
     */
    public function __construct(
        public string $message,
        public ResponsePayloadTypeEnum $type,
        public bool $closeModal,
        ?array $uniqKeys = null,
        ?EventId $sentryEventId = null
    ) {

        if ($uniqKeys === null || count($uniqKeys) === 0) {
            $this->uniqKeys = null;
            return;
        }

        $this->eventId = (string) $sentryEventId;

        $this->uniqKeys =
            array_values(
            array_map(static function(string|int $item) { return (string) $item; },
                array_filter($uniqKeys, static function(string|int|null $item) { return $item !== null; })));
    }
}