<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Communication\FrontHandlers;

use Contributte\Translation\Exceptions\InvalidArgument;
use Nette\Application\AbortException;

abstract class AFrontHandler {

    public function __construct(
        protected ?string $responsePayloadCustomClassName = '\GraphQLFrontApi\Communication\FrontHandlers\ResponsePayload') {
    }

    /**
     * Logout user if logged in
     * @param string $homePageLink - redirect link to homepage
     * @throws AbortException
     */
    public abstract function handleLogout(string $homePageLink): void;

    /**
     * Redirect client to some page
     * @param string $link - link definition
     * @throws AbortException
     */
    public abstract function handleRedirect(string $link): void;

    /**
     * Send response to FE with some message
     * @param ResponsePayload $responsePayload - payload object or child of custom object
     * @param bool $sendNow - send right now
     * @throws AbortException
     * @noinspection PhpUnused
     */
    public abstract function handleApiResponse(ResponsePayload $responsePayload, bool $sendNow = false): void;

    /**
     * Get ISO locale from user or etc.
     * @return string
     */
    public abstract function getLocale(): string;

    /**
     * Get user ID (if logged)
     * @return string|null
     */
    public abstract function getUserId(): string|null;

    /**
     * Get user data (if logged)
     * @return array|null
     */
    public abstract function getUserData(): array|null;

    /**
     * Get bearer token (if logged)
     * @return string|null
     */
    public abstract function getUserToken(): string|null;

    /**
     * Translate key into translated string
     * @param string $key - key based value for translation
     * @return string
     * @throws InvalidArgument
     */
    public abstract function translateKey(string $key): string;
}