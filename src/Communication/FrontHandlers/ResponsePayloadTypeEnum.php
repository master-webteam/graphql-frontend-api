<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Communication\FrontHandlers;

enum ResponsePayloadTypeEnum: string {

    case TYPE_INFO = 'info';
    case TYPE_ALERT = 'alert';
    case TYPE_SUCCESS = 'success';
    case TYPE_ERROR = 'error';
}
