<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Communication\FrontHandlers;

class InternalFrontHandlerWrapper {

    public function __construct(
        public ?array $customTranslationKeys,
        public ?array $customAutoResponsePayloadUniqCodeMapping,
        public AFrontHandler $frontHandler) { }
}