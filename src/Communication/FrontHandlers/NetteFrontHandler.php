<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Communication\FrontHandlers;

use Contributte\Translation\Exceptions\InvalidArgument;
use GraphQLFrontApi\BaseNettePresenter;
use JetBrains\PhpStorm\NoReturn;
use Nette\Application\AbortException;

class NetteFrontHandler extends AFrontHandler {

    public function __construct(
        private readonly BaseNettePresenter $presenter,
        ?string $responsePayloadCustomClassName = null) {

        // If custom payload class then use it
        parent::__construct();
        if ($responsePayloadCustomClassName !== null) {
            parent::__construct($responsePayloadCustomClassName);
        }
    }

    /** @throws AbortException */
    #[NoReturn] public function handleLogout(string $homePageLink): void {

        if (!$this->presenter->getUser()->isLoggedIn()) { return; }
        $this->presenter->getUser()->logout(true);
        $this->handleRedirect($homePageLink);
    }

    /** @throws AbortException */
    #[NoReturn] public function handleRedirect(string $link): void {
        $this->presenter->redirect($link);
    }

    /**
     * @throws AbortException
     */
    public function handleApiResponse(ResponsePayload $responsePayload, bool $sendNow = false): void {

        if (! $this->presenter->isAjax()) {
            $this->layoutFlashMessage($responsePayload);
            return;
        }

        $currentPayloads = $this->presenter->getPayload()->flashMessages ?? [];
        $currentPayloads[] = $responsePayload;
        $this->presenter->payload->flashMessages = $currentPayloads;
        if ($sendNow) {  $this->presenter->sendPayload(); }
    }

    /**
     * Saves the message to template, that can be displayed after redirect.
     * @param ResponsePayload $responsePayload
     * @noinspection PhpPossiblePolymorphicInvocationInspection
     */
    private function layoutFlashMessage(ResponsePayload $responsePayload): void {

            $id = $this->presenter->getParameterId('flash');
            $flash = $responsePayload;
            $messages = $this->presenter->getPresenter()->getFlashSession()->{$id};
            $messages[] = $flash;
            $this->presenter->getTemplate()->flashes = $messages;
            $this->presenter->getPresenter()->getFlashSession()->{$id} = $messages;
    }

    public function getLocale(): string {
        return $this->presenter->translator->getLocale();
    }

    public function getUserId(): string|null {

        $user = $this->presenter->getUser();
        if (!$user->isLoggedIn()) { return null; }
        return $user->getId() !== null ? (string) $user->getId() : null;
    }

    public function getUserToken(): string|null {

        $user = $this->presenter->getUser();
        if (!$user->isLoggedIn()) { return null; }

        $identity = $user->getIdentity();
        if ($identity === null) { return null; }

        $roles = $identity->getRoles();
        if (empty($roles[0])) { return null; }
        return $roles[0];
    }

    public function getUserData(): array|null {

        $user = $this->presenter->getUser();
        if (!$user->isLoggedIn()) { return null; }

        $identity = $user->getIdentity();
        return $identity?->getData();
    }

    /** @throws InvalidArgument */
    public function translateKey(string $key): string {
        return $this->presenter->translator->translate($key);
    }
}