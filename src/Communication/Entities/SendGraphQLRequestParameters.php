<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace GraphQLFrontApi\Communication\Entities;

class SendGraphQLRequestParameters extends SendRequestParameters {

    // GraphQL Query [Requested!]
    public string $query;

    /***************** Cache *****************/

    // Enable cache read try [Optional]
    public ?bool $enableCacheRead = null;

    // Valid seconds for the response [Default cache disabled]
    public ?int $validSeconds = null;

    // Invalidate this cache tags [Optional]
    public ?array $invalidateTags = null;

    // If some extra tags used in cache [Optional]
    public ?array $additionalCacheTags = null;

    // Disable cache divided per user [Optional]
    public ?bool $disableCachePerUser = null;

    // Disable cache divided per language [Optional]
    public ?bool $disableCachePerLanguage = null;

    /***************** Entities *****************/

    // Used in selectAll function to process max selection recursion level
    public ?int $selectRecursionFactor = null;

    // Used in selectAll function to process max selection recursion level (just for props)
    public ?array $selectConditionalRecursionFactor = null;

    // Used in selectAll function to whitelist just some props
    public ?array $selectForceWhiteList = null;

    // Implicitly say this is modifying mutation to prevent accidental null/undefined variables resolution
    public ?bool $setIsModifyRequest = null;

    public function __construct() {

        $this->type = new RequestTypes(RequestTypes::POST);
        $this->contentType = 'application/json';
    }
}