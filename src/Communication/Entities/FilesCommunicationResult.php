<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Communication\Entities;

readonly class FilesCommunicationResult {

    public function __construct(
        public string $content,
        public string|null $filename
    ) {}
}