<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace GraphQLFrontApi\Communication\Entities;

abstract class SendRequestParameters {

    /***************** Data *****************/

    // Ignore payload back from API [Optional]
    public ?bool $dontProcessResultData = null;

    // Don't try to process unauthorized [Optional]
    public ?bool $dontProcessUnauthorizedException = null;

    /***************** cURL *****************/

    // Request Type
    public RequestTypes $type;

    // Override presenter language [Default presenter language]
    public ?string $languageISO = null;

    // CURL timeout [Default 10]
    public int $curlTimeout = 10;

    // Set content type of request [Default JSON]
    public string $contentType;

    // Don't send accept language header [Optional]
    public ?bool $dontSendLanguageInRequest = null;

    // Don't send info from request in headers [Optional]
    public ?bool $dontSendRequestInfoInRequest = null;

    /***************** Auth *****************/

    // Specific access token [Optional]
    public ?string $bearerToken = null;

    // Specify to use bearer from user object in presenter [Optional]
    public bool $useBearerTokenFromPresenter = true;

    // If we need to manually set user id
    public ?string $forceUserId = null;
}