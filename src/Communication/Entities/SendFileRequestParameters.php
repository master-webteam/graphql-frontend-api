<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Communication\Entities;
use CURLStringFile;

class SendFileRequestParameters extends SendRequestParameters {

    // Extra form data [Requested!]
    public array $extraData = [];

    /**
     * Files form data [Requested!]
     * @var CURLStringFile[]|null
     */
    public ?array $files = null;

    /***************** cURL *****************/

    // Endpoint to be used outside GraphQL API
    public string $endpoint;

    /** @noinspection PhpUnused */
    public function __construct(bool $isUpload) {

        $this->type = $isUpload ? new RequestTypes(RequestTypes::POST) : new RequestTypes(RequestTypes::GET);
        $this->contentType = $isUpload ? 'multipart/form-data' : 'application/json';
        $this->dontProcessResultData = true;
        $this->dontProcessUnauthorizedException = true;
    }
}