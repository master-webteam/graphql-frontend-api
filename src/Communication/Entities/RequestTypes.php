<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace GraphQLFrontApi\Communication\Entities;

use MyCLabs\Enum\Enum;

/**
 * Class RequestTypes
 * @package GraphQLFrontApi
 */
class RequestTypes extends Enum {

    public const string POST = 'POST';

    public const string GET = 'GET';

    public const string DELETE = 'DELETE';

    public const string PUT = 'PUT';
}
