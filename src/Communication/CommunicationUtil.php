<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Communication;

use Nette\Http\Request;

class CommunicationUtil {

    // CloudFlare IP header
    private const string cloudFlareIPHeader = 'cf-connecting-ip';

    // CloudFlare Country header
    private const string cloudFlareCountryHeader = 'cf-ipcountry';

    // Proxy forward
    private const string forwardingHeader = 'x-forwarded-for';

    /**
     * Try to get client IP
     * @param Request $request
     * @return string
     */
    public static function getClientIp(Request $request): string {

        return $request->getHeader(self::cloudFlareIPHeader)
            ?? $request->getHeader(self::forwardingHeader)
            ?? $request->getRemoteAddress()
            ?? 'none';
    }

    public static function getClientCountry(Request $request): string {

        return $request->getHeader(self::cloudFlareCountryHeader) ?? 'unknown';
    }
}