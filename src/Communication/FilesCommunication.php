<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Communication;

use GraphQLFrontApi\Communication\Entities\FilesCommunicationResult;
use GraphQLFrontApi\Communication\Entities\RequestTypes;
use GraphQLFrontApi\Communication\Entities\SendFileRequestParameters;
use GraphQLFrontApi\Entities\GuzzleHandle;
use GraphQLFrontApi\Exceptions\Communication\GraphQLApiException;
use GraphQLFrontApi\Exceptions\Communication\GraphQLApiInternalException;
use GraphQLFrontApi\Exceptions\Communication\GraphQLApiNotFoundException;
use GraphQLFrontApi\Exceptions\Communication\GraphQLApiUnauthorizedException;
use GraphQLFrontApi\Exceptions\GraphQLCodeException;
use Nette\Application\AbortException;

class FilesCommunication extends Communication {

    // Contains request parameters [PARAM]
    private SendFileRequestParameters $specificParameters;

    private const string FILENAME_CONTENT_DISPOSITION_REGEX = '/filename=\"(.*?)\"/m';

    /** @noinspection PhpUnused */
    public function setFilesDomain(?string $filesDomain): void { $this->filesDomain = $filesDomain; }

    /**
     * @param SendFileRequestParameters $parameters
     * @return FilesCommunicationResult|null
     * @throws GraphQLApiException
     * @throws GraphQLApiInternalException
     * @throws GraphQLApiNotFoundException
     * @throws GraphQLApiUnauthorizedException
     * @throws GraphQLCodeException
     * @noinspection PhpUnused
     */
    public function sendRequestFile(SendFileRequestParameters $parameters): ?FilesCommunicationResult {

        $this->requestParameters = $parameters;
        $this->specificParameters = $parameters;
        $this->initializeDebugObject($this->logCallStack);
        $this->checkPreConditions();
        $this->initializePropertiesFromPresenter();
        $this->initializeGuzzleRequest();
        $this->addPostFieldsToRequest();
        $this->addAuthFieldsToRequest();
        $this->addLangFieldsToRequest();
        $this->addRemoteInfoFieldsToRequest();
        $this->sendFilesRequest();
        $this->processErrors();
        $this->debugFiles();

        return is_string($this->responseBodyContent)
            ? new FilesCommunicationResult(
                content: $this->responseBodyContent,
                filename: ($this->responseHeaders['Content-Disposition'][0] ?? null) === null
                    ? null
                    : self::parseFilenameFromContentDisposition($this->responseHeaders['Content-Disposition'][0])
            )
            : null;
    }

    /**
     * Function to parse content-disposition header to get filename
     * @param string $contentDisposition - content disposition header content
     * @return string|null - parsed filename
     */
    private static function parseFilenameFromContentDisposition(string $contentDisposition): ?string {

        preg_match(self::FILENAME_CONTENT_DISPOSITION_REGEX, $contentDisposition, $output);
        if (!isset($output[1])) { return null;}
        return urldecode($output[1]);
    }

    /** Initialize output data to send File */
    function addPostFieldsToRequest(): void {

        $this->guzzleHandle->requestType = $this->specificParameters->type;
        $this->guzzleHandle->headers['Content-Type'] = $this->specificParameters->contentType;
    }

    /**
     * Update debug object with file request parameters
     */
    protected function initializeDebugObject(bool $logCallStack): void {

        parent::initializeDebugObject($logCallStack);
        $this->debugObject->canonizedIdentify = $this->specificParameters->endpoint;
        $this->debugObject->parameters = $this->specificParameters;
        $this->debugObject->communicatorDebug->domain = $this->filesDomain;
    }

    /** Initialize Guzzle API */
    function initializeGuzzleRequest(): void {
        $this->guzzleHandle = new GuzzleHandle(uri: $this->filesDomain . '/' . $this->specificParameters->endpoint);
    }

    /**
     * Files API URL
     * @return string|null
     * @noinspection PhpUnused
     */
    function getApiAddress(): string|null {
        return $this->filesDomain;
    }

    /**
     * Send GraphQL request
     * @throws AbortException
     * @throws GraphQLCodeException
     * @throws GraphQLApiInternalException
     * @throws GraphQLApiUnauthorizedException
     * @throws GraphQLApiNotFoundException
     */
    protected function sendFilesRequest(): void {

        if ($this->specificParameters->type->getValue() === RequestTypes::GET) {
            $this->sendRequest($this->specificParameters->endpoint, $this->specificParameters->endpoint);
            return;
        }

        unset($this->guzzleHandle->headers['Content-Type']);
        $this->guzzleHandle->multipartOptions = $this->getOptionsMultipart();
        $this->sendRequest($this->specificParameters->endpoint, $this->specificParameters->endpoint);
    }

    /**
     * Debug Files request
     */
    protected function debugFiles(): void {
        $this->debug($this->specificParameters->endpoint);
    }

    /**
     * Construct multipart blocks
     * @return array - Guzzle multipart configuration
     */
    private function getOptionsMultipart(): array {

        // Append extra data
        $optionsMultipart = [];
        foreach ($this->specificParameters->extraData as $extraName => $extraData) {
            $optionsMultipart[] = [
                'name' => $extraName,
                'contents' => $extraData
            ];
        }

        // Append files
        foreach ($this->specificParameters->files as $file) {
            $optionsMultipart[] = [
                'name' => 'files',
                'contents' => $file->data,
                'filename' => $file->postname,
                'headers' => ['Content-Type' => $file->mime]
            ];
        }

        return $optionsMultipart;
    }
}