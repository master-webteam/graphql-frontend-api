<?php

declare(strict_types=1);

namespace GraphQLFrontApi;

use Exception;
use MyCLabs\Enum\Enum;

/**
 * Class TestCreator
 * @package GraphQLFrontApi
 */
class TestCreator extends BaseGraphQL {

    private bool $alsoNullValues;

    /** TestCreator constructor. */
    public function __construct() {

        $this->alsoNullValues = false;
    }

    /** Test null values
     * @noinspection PhpUnused
     */
    public function setAlsoNullValues(): void {

        $this->alsoNullValues = true;
    }

    /**
     * @param BaseGraphQLEntity $entity
     * @param string $location
     * @throws Exception
     * @noinspection PhpUnused
     */
    public function processEquivalencies(BaseGraphQLEntity $entity, string $location = 'result'): void {

        // We have collection in the input -> move deeper!
        if (property_exists($entity, 'list')) {

            if (!isset($entity->list[0])) {
                return;
            }
            /** @noinspection PhpParamsInspection */
            $this->processEquivalencies($entity->list[0], $location . '->list[0]');
            return;
        }

        // Look for entity parameters
        foreach ($entity->getObjectProps() as $parameter => $value) {

            // Atomic value
            if (!$value instanceof BaseGraphQLEntity) {
                if (is_object($value)) {
                    if ($value instanceof Enum) {
                        $this->printEquivalencies($location . "->{$parameter}" . '->getValue()', gettype($value), "'" . $value->getValue() . "'");
                    }
                } else {
                    // phpcs:disable
                    switch (gettype($value)) {
                        case 'string' :
                            $this->printEquivalencies($location . "->{$parameter}", gettype($value), "'" . $value . "'");
                            break;
                        case 'double':
                        case 'integer' :
                            $this->printEquivalencies($location . "->{$parameter}", gettype($value), (string) $value);
                            break;
                        default :
                            break;
                    }
                    // phpcs:enable
                }
                continue;
            }

            // Another entity
            $this->processEquivalencies($value, $location . "->{$parameter}");
        }
    }

    /**
     * @param BaseGraphQLEntity|Enum $entity
     * @param int $currentDepth
     * @param string $location
     * @param int $maxDepth
     * @throws Exception
     * @noinspection PhpUnused
     */
    public function processAssertions(BaseGraphQLEntity|Enum $entity, int $currentDepth = 0, string $location = 'result', int $maxDepth = 3): void {

        // We are on this node, need to print it
        $this->printAssertion($location, get_class($entity));

        // We have collection in the input -> move deeper!
        if (property_exists($entity, 'list')) {

            if (!isset($entity->list[0])) {
                return;
            }
            if ($currentDepth < $maxDepth) {
                /** @noinspection PhpParamsInspection */
                $this->processAssertions($entity->list[0], $currentDepth + 1, $location . '->list[0]', $maxDepth);
            }
            return;
        }

        // Enum
        if ($entity instanceof Enum) {
            return;
        }

        // Look for entity parameters
        foreach ($entity->getObjectProps() as $parameter => $value) {

            // Atomic value
            if (!$value instanceof BaseGraphQLEntity) {
                if (is_object($value)) {
                    $this->printAssertion($location . "->{$parameter}", get_class($value));
                } else {
                    $this->printAssertion($location . "->{$parameter}", gettype($value));
                }
                continue;
            }

            // Another entity
            if ($currentDepth < $maxDepth) {
                $this->processAssertions($value, $currentDepth + 1, $location . "->{$parameter}", $maxDepth);
            }
        }
    }

    /**
     * @param string $location
     * @param string $type
     * @throws Exception
     */
    private function printAssertion(string $location, string $type): void {

        if (!$this->alsoNullValues && $type === 'NULL') {
            return;
        }
        if ($type === 'boolean') {
            $type = 'bool';
        }
        if ($type === 'double') {
            $type = 'float';
        }
        echo "Assert::type('{$type}', \${$location}, '{$location}');\n";
    }

    /**
     * @param string $location
     * @param string $type
     * @param string $value
     * @throws Exception
     */
    private function printEquivalencies(string $location, string $type, string $value): void {

        $randomIdentifier = self::randHash();
        if (!$this->alsoNullValues && $type === 'NULL') {
            return;
        }
        if ($type === 'double') {
            $value = "(float){$value}";
        }

        echo "Assert::equal({$value}, \${$location}, '{$randomIdentifier}');\n";
    }

    /**
     * @return string
     * @throws Exception
     */
    private static function randHash(): string {

        return substr(md5(random_bytes(20)), -(32));
    }

    /**
     * Just for sniffers
     * @noinspection PhpUnused
     */
    public function justInit(): void {
    }
}