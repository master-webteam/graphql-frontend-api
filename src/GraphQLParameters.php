<?php

declare(strict_types=1);

namespace GraphQLFrontApi;

use GraphQLFrontApi\Generators\Entities\Schema;
use GraphQLFrontApi\Generators\Entities\SchemaWithURI;

/**
 * Class GraphQLParameters
 * @author Martin Pavelka
 * @package App\Models\System
 */
class GraphQLParameters extends BaseGraphQL {

    public ?array $graphQLParameters = null;

    /** @noinspection PhpUnused */
    public function setParameters(array $parameters): void {

        $this->graphQLParameters = $parameters;
    }

    /**
     * @return Schema[]
     * @noinspection PhpUnused
     */
    public function getSchemas(): array {

        $outputSchemas = [];
        foreach ($this->graphQLParameters as $backend) {
            $outputSchemas[] = new Schema($backend['schemaVersion'], $backend['schemaFile'], $backend['schemaDir']);
        }
        return $outputSchemas;
    }

    /**
     * @return SchemaWithURI[]
     * @noinspection PhpUnused
     */
    public function getSchemasWithUri(): array {

        $outputSchemas = [];
        foreach ($this->graphQLParameters as $backend) {
            $outputSchemas[] = new SchemaWithURI($backend['schemaVersion'], $backend['schemaFile'], $backend['schemaDir'], $backend['apiSchema']);
        }
        return $outputSchemas;
    }
}