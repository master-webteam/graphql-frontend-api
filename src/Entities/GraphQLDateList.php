<?php

/** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */

declare(strict_types=1);

namespace GraphQLFrontApi\Entities;

use GraphQLFrontApi\BaseGraphQLEntity;

/**
 * Class GraphQLDate
 * @package GraphQLFrontApi
 */
class GraphQLDateList extends BaseGraphQLEntity {

    /** @var \GraphQLFrontApi\Entities\GraphQLDate[] */
    public array $list = [];
}