<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Entities;

class ApiParsedErrorLocation {

    public ?int $line = null;

    public ?int $column = null;
}