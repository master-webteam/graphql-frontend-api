<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Entities;

use DateMalformedStringException;
use DateTime;
use Exception;

/**
 * Class GraphQLDate
 * @package GraphQLFrontApi
 */
class GraphQLDate extends GraphQLDates {

    /**
     * Convert datetime into custom graphql date
     * @param DateTime $instance
     * @return GraphQLDate
     * @throws Exception
     */
    public static function convertToGraphQLDate(DateTime $instance): self {

        $converted = new self();
        $converted->setTimestamp($instance->getTimestamp());
        return $converted;
    }

    /**
     * Create next day date
     * @return GraphQLDate
     * @throws DateMalformedStringException
     * @noinspection PhpUnused
     */
    public static function useNextDayGraphQLDate(): self {

        $instance = new DateTime();
        $instance->modify('+1 day');
        $converted = new self();
        $converted->setTimestamp($instance->getTimestamp());
        return $converted;
    }
}