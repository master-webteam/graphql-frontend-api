<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Entities;

use GraphQLFrontApi\Communication\Entities\RequestTypes;

class GuzzleHandle {

    public array $headers = [];

    public RequestTypes $requestType;

    public ?string $requestBody = null;

    public ?array $multipartOptions = null;

    public function __construct(public string $uri) {

    }
}