<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Entities;

enum GraphQLDateGeneratedType {

    case MONTH_AGO;

    case YEAR_AGO;

    case WEEK_AGO;

    case DAY_AGO;

    case HOUR_AGO;

    case DUMMY_OLD;

    case ACTUAL;

    case MONTH_END;

    case TWO_MONTHS_AGO;
}
