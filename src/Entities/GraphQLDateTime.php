<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Entities;

use DateMalformedStringException;
use DateTime;
use Exception;

/**
 * Class GraphQLDateTime
 * @package GraphQLFrontApi
 */
class GraphQLDateTime extends GraphQLDates {

    /**
     * Convert datetime into custom graphql date
     * @param DateTime $instance
     * @return GraphQLDateTime
     * @throws Exception
     */
    public static function convertToGraphQLDate(DateTime $instance): self {

        $converted = new self();
        $converted->setTimestamp($instance->getTimestamp());
        return $converted;
    }

    /**
     * Create last month datetime in this class format
     * @param GraphQLDateGeneratedType $type
     * @return GraphQLDateTime
     * @throws DateMalformedStringException
     * @noinspection PhpUnused
     */
    public static function generateDate(GraphQLDateGeneratedType $type): self {

        $instance = new DateTime();
        $converted = new self();
        $converted->setTimestamp($instance->getTimestamp());

        if ($type === GraphQLDateGeneratedType::ACTUAL) {
            return $converted;
        }

        /** @noinspection PhpUncoveredEnumCasesInspection */
        $modifier = match($type) {
            GraphQLDateGeneratedType::DAY_AGO => '-1 day',
            GraphQLDateGeneratedType::HOUR_AGO => '-1 hour',
            GraphQLDateGeneratedType::MONTH_AGO => '-1 month',
            GraphQLDateGeneratedType::WEEK_AGO => '-1 week',
            GraphQLDateGeneratedType::DUMMY_OLD => '-20 years',
            GraphQLDateGeneratedType::YEAR_AGO => '-1 year',
            GraphQLDateGeneratedType::TWO_MONTHS_AGO => '-2 months',
            GraphQLDateGeneratedType::MONTH_END => 'last day of this month',
        };

        $converted->modify($modifier);
        return $converted;
    }

    /** @noinspection PhpUnused */
    public function isInFuture(): bool {

        $currentTimeStamp = new DateTime()->getTimestamp();
        return $this->getTimestamp() > $currentTimeStamp;
    }

    /**
     * @noinspection PhpUnused
     * @throws DateMalformedStringException
     */
    public function isInPast(bool $oneDayReserve = false): bool {

        $current = new DateTime();
        if ($oneDayReserve) { $current->modify('-1 day'); }
        $currentTimeStamp = $current->getTimestamp();
        return $this->getTimestamp() < $currentTimeStamp;
    }
}