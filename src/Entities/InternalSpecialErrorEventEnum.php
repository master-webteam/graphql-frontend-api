<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Entities;

enum InternalSpecialErrorEventEnum: string {

    case TIMEOUT = 'timeout';
}
