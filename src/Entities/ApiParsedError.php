<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Entities;

class ApiParsedError {

    // Api timeStamp for response f.ex. 1617905887681
    public ?int $timeStamp = null;

    // Api status code f.ex. 401
    public ?int $httpStatus = null;

    // Api message f.ex. Token has expired.
    public ?string $message = null;

    // Api error code f.ex. Unauthorized
    public ?string $error = null;

    // Error type f.ex. PAYMENT_STATUS_ERROR
    public ?string $errorType = null;

    // Error description
    public ?string $descriptionError = null;

    // Error classification f.ex. DataFetchingException
    public ?string $classification = null;

    // Error classification f.ex. ValidationException
    public ?string $objectClass = null;

    /**
     * List of violation in validation exception
     * @var ApiParsedErrorViolation[]|null
     */
    public ?array $violations = null;

    /**
     * Api path for exception f.ex. ["/apiCis3/graphql"]
     * @var string[]|null
     */
    public ?array $paths = null;

    /**
     * Api error locations f.ex. [{"line":1,"column":9}]
     * @var ApiParsedErrorLocation[]|null
     */
    public ?array $locations = null;
}