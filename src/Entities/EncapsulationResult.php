<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Entities;

class EncapsulationResult {

    public function __construct(public string $resultQuery, public array $queryNameList) {}
}