<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Entities;

class ApiParsedErrorViolation {

    // F.ex. DNSRecord
    public ?string $entityClassName = null;

    public ?string $arguments = null;

    // F.ex. INVALID
    public ?string $type = null;

    // F.ex. A
    public ?string $parameter = null;

    // F.ex. ipv4MustBe
    public ?string $uniqueKey = null;
}