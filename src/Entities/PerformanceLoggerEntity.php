<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Entities;

class PerformanceLoggerEntity {

    public int $curlFirstTime;

    public int $curlTotalTime;

    public string $query;

    public ?string $visitedURL = null;

    public ?string $userIdentify = null;

    public ?int $timeStamp = null;

    public ?string $baseQuery = null;

    public function __toString(): string {

        return $this->query;
    }
}