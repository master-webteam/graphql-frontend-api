<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Entities;

use DateTime;
use Exception;

/**
 * Class GraphQLDates
 * @package GraphQLFrontApi
 */
class GraphQLDates extends DateTime {

    /**
     * @return int
     * @throws Exception
     * @noinspection PhpUnused
     */
    public static function getSecondsToHourEnd(): int {

        $current = new DateTime();
        $instance = new DateTime();
        $converted = new DateTime();
        $converted->setTimestamp($instance->getTimestamp());
        $converted->setTime((int) $converted->format('H'), 59, 59);
        return abs($current->getTimestamp() - $converted->getTimestamp());
    }

    /**
     * @return int
     * @throws Exception
     * @noinspection PhpUnused
     */
    public static function getSecondsToMonthEnd(): int {

        $current = new DateTime();
        $instance = new DateTime();
        $converted = new DateTime();
        $converted->setTimestamp($instance->getTimestamp());
        $converted->modify('last day of this month');
        $converted->setTime(23, 59, 59);
        return abs($current->getTimestamp() - $converted->getTimestamp());
    }

    /**
     * @return int
     * @throws Exception
     * @noinspection PhpUnused
     */
    public static function getSecondsToDayEnd(): int {

        $current = new DateTime();
        $instance = new DateTime();
        $converted = new DateTime();
        $converted->setTimestamp($instance->getTimestamp());
        $converted->setTime(23, 59, 59);
        return abs($current->getTimestamp() - $converted->getTimestamp());
    }

    /**
     * @return int
     * @throws Exception
     * @noinspection PhpUnused
     */
    public static function getSecondsGraphDaily(): int {

        $current = new DateTime();
        $instance = new DateTime();
        $converted = new DateTime();
        $minutesDivided = (int) $converted->format('i') === 0 ? 0 : (int) $converted->format('i') / 5;
        $converted->setTimestamp($instance->getTimestamp());
        $converted->setTime((int) $converted->format('H'), (int) ceil($minutesDivided * 5));
        return abs($current->getTimestamp() - $converted->getTimestamp());
    }

    /**
     * @return int
     * @throws Exception
     * @noinspection PhpUnused
     */
    public static function getSecondsGraphWeekly(): int {

        $current = new DateTime();
        $instance = new DateTime();
        $converted = new DateTime();
        $converted->setTimestamp($instance->getTimestamp());
        $converted->setTime((int) $converted->format('H'), 00, 00);
        return abs($current->getTimestamp() - $converted->getTimestamp());
    }

    /**
     * @return int
     * @throws Exception
     * @noinspection PhpUnused
     */
    public static function getSecondsGraphMonthly(): int {

        $current = new DateTime();
        $instance = new DateTime();
        $converted = new DateTime();
        $hoursDivided = (int) $converted->format('H') === 0 ? 0 : (int) $converted->format('H') / 4;
        $converted->setTimestamp($instance->getTimestamp());
        $converted->setTime((int) ceil($hoursDivided * 4), 00, 00);
        return abs($current->getTimestamp() - $converted->getTimestamp());
    }

    /**
     * @return int
     * @throws Exception
     * @noinspection PhpUnused
     */
    public static function getSecondsGraphYearly(): int {

        $current = new DateTime();
        $instance = new DateTime();
        $converted = new DateTime();
        $converted->setTimestamp($instance->getTimestamp());
        $converted->modify('+1 day');
        $converted->setTime(0, 0);
        return abs($current->getTimestamp() - $converted->getTimestamp());
    }

    /**
     * @return int
     * @noinspection PhpUnused
     */
    public static function getSeconds10Minutes(): int {

        return 10 * 60;
    }

    /**
     * @return int
     * @noinspection PhpUnused
     */
    public static function getSeconds5Minutes(): int {

        return 5 * 60;
    }

    /**
     * @return int
     * @noinspection PhpUnused
     */
    public static function getSecondsHour(): int {

        return 60 * 60;
    }

    /**
     * @return int
     * @noinspection PhpUnused
     */
    public static function getSecondsWorkDay(): int {

        return 4 * 60 * 60;
    }

    /**
     * @return int
     * @noinspection PhpUnused
     */
    public static function getSecondsDay(): int {

        return 24 * 60 * 60;
    }

    /**
     * @return int
     * @noinspection PhpUnused
     */
    public static function getSecondsMonth(): int {

        return 31 * 24 * 60 * 60;
    }

    /**
     * @return int
     * @noinspection PhpUnused
     */
    public static function getSecondsRemoveASAP(): int {

        return 60;
    }
}