<?php

declare(strict_types=1);

namespace GraphQLFrontApi;

use GraphQLFrontApi\Communication\Entities\SendGraphQLRequestParameters;
use GraphQLFrontApi\Entities\EncapsulationResult;
use GraphQLFrontApi\Entities\GraphQLDate;
use GraphQLFrontApi\Entities\GraphQLDateTime;
use GraphQLFrontApi\Exceptions\GraphQLCodeException;
use GraphQLFrontApi\Exceptions\GraphQLSimpleCodeException;
use MyCLabs\Enum\Enum;

/**
 * Class GraphQLCreator
 * @author Martin Pavelka
 * @package App\Models\System
 */
class GraphQLCreator extends BaseGraphQL {

    /**
     * Select part
     * @param array $selectingParameters - selecting parameters
     * @param BaseGraphQLEntity $resourceObject
     * @return string - selecting query
     * @throws GraphQLCodeException
     * @throws GraphQLSimpleCodeException
     * @throws GraphQLSimpleCodeException
     * @throws GraphQLSimpleCodeException
     * @example { prop1, prop2, propRecursive { prop1, prop2 } }
     */
    private function produceSelectQuery(array $selectingParameters, BaseGraphQLEntity $resourceObject): string {

        // Produce query prefix
        if (empty($selectingParameters)) { return ''; }
        $outputString = '{';
        $resourceClass = $resourceObject::class;
        $resourceObjectProps = $resourceObject->getObjectProps();

        // Now process all props marked for select
        foreach ($selectingParameters as $selectingParameter) {

            // We have used - this will be skipped
            if (str_contains($selectingParameter, self::usedParameterPrefix)) { continue; }

            // Read a property type
            $parameterType = $this->getPropertyType($resourceClass, $selectingParameter);
            $parameterValue = $resourceObjectProps[$selectingParameter];

            // Own entity
            if (is_subclass_of($parameterType, BaseGraphQLEntity::class)) {

                // Check sub select object exists
                if ($resourceObject->{$selectingParameter} === null) {
                    throw new GraphQLSimpleCodeException(message: "Missing sub-selection object for {$selectingParameter} in " . $resourceObject::class);
                }

                $subEntitySelectParameters = $this->getSelectingParameters($resourceObject->{$selectingParameter});
                $subEntityParameter = $resourceObject->{$selectingParameter};

                // We have collection with selecting info in first list item (which is entity)
                if (isset($subEntityParameter->list[0]) && $subEntityParameter->list[0] instanceof BaseGraphQLEntity) {
                    $subEntitySelectParameters = $this->getSelectingParameters($subEntityParameter->list[0]);
                    $subEntityParameter = $subEntityParameter->list[0];
                }

                // We have list with enums (no selection info needed - just use it in query)
                if (isset($subEntityParameter->usedClassInList) && self::checkGivenClassDocContainsString($subEntityParameter->usedClassInList, '@description enum')) {
                    $outputString .= "{$selectingParameter} ";
                    continue;
                }

                // We don't have any selecting info inside list
                if (empty($subEntitySelectParameters)) {
                    continue;
                }

                // We have object directly
                $recursionSelectingQuery = $this->produceSelectQuery($subEntitySelectParameters, $subEntityParameter);
                $outputString .= "{$selectingParameter}{$recursionSelectingQuery} ";
                continue;
            }

            // Datetime entity
            if ($parameterType === GraphQLDate::class || $parameterType === GraphQLDateTime::class) {
                $outputString .= "{$selectingParameter} ";
                continue;
            }

            // Array - we define recursion in first item, object is direct
            $temporaryParameterValue = is_object($parameterValue) ? $parameterValue : null;
            if (is_array($parameterValue) && current($parameterValue) !== false) {
                $temporaryParameterValue = current($parameterValue);
            }

            if ($temporaryParameterValue !== null && is_subclass_of($temporaryParameterValue, '\GraphQLFrontApi\BaseGraphQlEntity')) {
                $outputString .= $this->getQueryFromObjectWithForceName($temporaryParameterValue, $selectingParameter);
                continue;
            }

            // Other just append
            $outputString .= "{$selectingParameter} ";
        }

        // Just close the query
        return rtrim($outputString, ' ') . '}';
    }

    /**
     * Data part
     * @param array $filteringParameters - filtering parameters
     * @return string - the filtering part of the query
     * @example (fromTime: "2018-01-01T00:00",toTime: "2018-01-31T23:59",userID: 172)
     */
    private function produceFilterQuery(array $filteringParameters): string {

        // Produce query prefix
        $outputString = '';
        if (empty($filteringParameters)) {
            return $outputString;
        }

        // Now process all props marked as data
        foreach ($filteringParameters as $filteringParameterKey => $filteringParameterValue) {

            // We have used - this will be skipped
            if (str_contains($filteringParameterKey, self::usedParameterPrefix)) {
                continue;
            }

            // Enum
            if ($filteringParameterValue instanceof Enum) {
                $outputString .= "{$filteringParameterKey}: " . $filteringParameterValue->getValue() . ',';
                continue;
            }

            // DateTime
            if ($filteringParameterValue instanceof GraphQLDateTime) {
                $formattedDate = $filteringParameterValue->format('Y-m-d\TH:i:s');
                $outputString .= "{$filteringParameterKey}: \"" . $formattedDate . '",';
                continue;
            }

            // Date
            if ($filteringParameterValue instanceof GraphQLDate) {
                $formattedDate = $filteringParameterValue->format('Y-m-d');
                $outputString .= "{$filteringParameterKey}: \"" . $formattedDate . '",';
                continue;
            }

            // Float
            if (is_float($filteringParameterValue)) {
                $formattedDouble = number_format($filteringParameterValue, 10, '.', '');
                $outputString .= "{$filteringParameterKey}: " . $formattedDouble . ',';
                continue;
            }

            // Array or collection
            if (is_array($filteringParameterValue) || (is_object($filteringParameterValue) && property_exists($filteringParameterValue, 'list'))) {

                // Convert collection
                if (is_object($filteringParameterValue)) {
                    $filteringParameterValue = $filteringParameterValue->list;
                }

                // Empty
                if (empty($filteringParameterValue)) {
                    $outputString .= "{$filteringParameterKey}:[],";
                    continue;
                }

                // Process items
                $arrayOutputString = "{$filteringParameterKey}:[";
                foreach ($filteringParameterValue as $item) {

                    // Enums in array
                    if ($item instanceof Enum) {
                        $arrayOutputString .= $item->getValue() . ',';
                    } // Object in array
                    else if (is_subclass_of($item, BaseGraphQLEntity::class)) {
                        $subObjectFilteringParams = $this->getFilteringParameters($item);
                        $subObjectFilteringQuery = $this->produceFilterQuery($subObjectFilteringParams);
                        $arrayOutputString .= self::appendBrackets($subObjectFilteringQuery, true) . ',';
                    } // Basic type in array
                    else {
                        $arrayOutputString .= match (get_debug_type($item)) {
                            'int' => "{$item},",
                            'float' => number_format($item, 10, '.', '') . ',',
                            'string' => "\"{$item}\",",
                            default => ''
                        };
                    }
                }

                $outputString .= rtrim($arrayOutputString, ',') . '],';
                continue;
            }

            // Object
            if ($filteringParameterValue instanceof BaseGraphQLEntity) {
                $subObjectFilteringParams = $this->getFilteringParameters($filteringParameterValue);
                $subObjectFilteringQuery = $this->produceFilterQuery($subObjectFilteringParams);
                if (count($subObjectFilteringParams) === 0) {
                    continue;
                }
                $outputString .= "{$filteringParameterKey}:" . self::appendBrackets($subObjectFilteringQuery, true) . ',';
                continue;
            }

            // Bool
            if (is_bool($filteringParameterValue)) {
                $filteringParameterValueBool = ($filteringParameterValue) ? 'true' : 'false';
                $outputString .= "{$filteringParameterKey}: {$filteringParameterValueBool},";
                continue;
            }

            // String
            if (is_string($filteringParameterValue)) {
                $filteringParameterValue = self::removeMessFromStringValue($filteringParameterValue);
                $outputString .= "{$filteringParameterKey}: \"{$filteringParameterValue}\",";
                continue;
            }

            // Null
            if ($filteringParameterValue === null) {
                $outputString .= "{$filteringParameterKey}: null,";
                continue;
            }

            // Other
            $outputString .= "{$filteringParameterKey}: {$filteringParameterValue},";
        }

        // Just finish query
        return rtrim($outputString, ',');
    }

    /**
     * Produce query for the object
     * @param BaseGraphQLEntity $filterResource
     * @param BaseGraphQLEntity $selectResource
     * @param SendGraphQLRequestParameters $parameters
     * @return string - [query => "producedQuery"]
     * @throws GraphQLCodeException
     * @throws GraphQLSimpleCodeException
     * @noinspection PhpUnused
     */
    public function getQueryFromObject(BaseGraphQLEntity $filterResource, BaseGraphQLEntity $selectResource, SendGraphQLRequestParameters $parameters): string {

        $selectingParameters = $this->getSelectingParameters($selectResource);
        $filteringParameters = $this->getFilteringParameters($filterResource);
        $this->checkFilteringParameters($filterResource, $parameters->setIsModifyRequest === true);
        return self::camelCase($filterResource->usedNameInQuery)
            . self::appendBrackets($this->produceFilterQuery($filteringParameters))
            . $this->produceSelectQuery($selectingParameters, $selectResource);
    }

    /**
     * Ups, redundant but used by recursion in filtering :(
     * @param BaseGraphQLEntity $resourceObject
     * @param string $name - name of the prop used for the name
     * @return string - sub query here
     * @throws GraphQLCodeException
     * @throws GraphQLSimpleCodeException
     * @noinspection PhpUnused
     */
    private function getQueryFromObjectWithForceName(BaseGraphQLEntity $resourceObject, string $name): string {

        $selectingParameters = $this->getSelectingParameters($resourceObject);
        $filteringParameters = $this->getFilteringParameters($resourceObject);
        $entityName = $name;
        return $entityName . self::appendBrackets($this->produceFilterQuery($filteringParameters))
            . $this->produceSelectQuery($selectingParameters, $resourceObject);
    }

    /**
     * Pack queries into one query or mutation
     * @param array $queries
     * @param bool $isMutation
     * @return EncapsulationResult
     * @throws GraphQLSimpleCodeException
     */
    public function encapsulateQueries(array $queries, bool $isMutation): EncapsulationResult {

        // Check if any query name used more than once
        // Create array for current indexes
        $queryNameOccurrences = [];
        $queryNameIndexes = [];
        foreach ($queries as $query) {
            $queryName = Utils::getQueryName($query);
            $queryNameOccurrences[$queryName] = array_key_exists($queryName, $queryNameOccurrences) ? $queryNameOccurrences[$queryName] + 1 : 1;
            $queryNameIndexes[$queryName] = 1;
        }

        // Prepare for compose
        $typeName = ($isMutation) ? 'mutation' : 'query';
        $prefix = "{$typeName} { ";
        $suffix = '}';
        $output = $prefix;

        // Add queries inside
        $queryNameList = [];
        foreach ($queries as $query) {
            $queryName = Utils::getQueryName($query);
            if ($queryNameOccurrences[$queryName] > 1) {
                $output .= Utils::getNamedByIndexQuery($query, $queryNameIndexes[$queryName]);
                $queryNameList[] = $queryName . $queryNameIndexes[$queryName];
                ++$queryNameIndexes[$queryName];
            } else {
                $output .= $query;
                $queryNameList[] = $queryName;
            }
            $output .= ' ';
        }

        // Create output
        return new EncapsulationResult(resultQuery: $output . $suffix, queryNameList: $queryNameList);
    }
}