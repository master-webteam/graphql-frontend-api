<?php

declare(strict_types=1);

namespace GraphQLFrontApi;

use GraphQLFrontApi\Exceptions\GraphQLCodeException;
use GraphQLFrontApi\Exceptions\GraphQLSimpleCodeException;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;

/**
 * Class BaseGraphQL
 * @package GraphQLFrontApi
 */
class BaseGraphQL {

    /** @var string - prefix used when selecting properties */
    public const string selectedParameterPrefix = 'select';

    /** @var string - prefix used when forcing null to parameters */
    public const string forcedParameterPrefix = 'forced';

    /** @var string - prefix for the entity name to be used in the query */
    public const string usedParameterPrefix = 'usedName';

    /**
     * Remove illegal or wrong characters from strings
     * @param string $value - input string
     * @return string - filtered string
     */
    protected static function removeMessFromStringValue(string $value): string {

        return str_replace(["\n", "\'", '\"', "\r"], ['\\n', '\\"', '\\"', ''], addslashes($value));
    }

    /**
     * Convert string to camel case
     * @param string $inputString
     * @return string - camelCase string
     */
    public static function camelCase(string $inputString): string {

        $outStr = preg_replace('/[^a-z0-9]+/i', ' ', $inputString);
        $outStr = ucwords(trim($outStr));
        $outStr = str_replace(' ', '', $outStr);
        return lcfirst($outStr);
    }

    /**
     * Get the object properties which contains the selected prefix string
     * @param string $objectParameter - object property name
     * @return bool - true if selected prefix found
     * @noinspection PhpUnused
     */
    protected static function getCalledSelected(string $objectParameter): bool {

        return str_contains($objectParameter, self::selectedParameterPrefix)
            && strlen($objectParameter) > 6
            && ctype_upper($objectParameter[6]);
    }

    /**
     * Get the object properties which contains the forced prefix string
     * @param string $objectParameter - object property name
     * @return bool - true if forced prefix found
     * @noinspection PhpUnused
     */
    protected static function getCalledForced(string $objectParameter): bool {

        return str_contains($objectParameter, self::forcedParameterPrefix)
            && strlen($objectParameter) > 6
            && ctype_upper($objectParameter[6]);
    }

    /**
     * Remove prefix from the selected parameter name and convert to camel case
     * @param string $selectPrefixedParameterName - original property name
     * @return string - updated name without selected and camelCase
     * @noinspection PhpUnused
     */
    protected static function removeSelectedPrefixAndCamelCase(string $selectPrefixedParameterName): string {
        return self::camelCase(substr($selectPrefixedParameterName, strlen(self::selectedParameterPrefix)));
    }

    /**
     * Get params for the filtering (without select prefix and without null)
     * @param $filterParameterValue - property value
     * @param string $filterParameterName - property name
     * @return bool - true if it is filtering one
     * @noinspection PhpUnused
     */
    protected static function getFilterParams($filterParameterValue, string $filterParameterName): bool {

        return !str_contains($filterParameterName, self::selectedParameterPrefix)
            && !str_contains($filterParameterName, self::forcedParameterPrefix)
            && $filterParameterValue !== null;
    }

    /**
     * Just append brackets
     * @param string $bracketContent - content in the brackets
     * @param bool $curved - use curved brackets
     * @param bool $removeEmpty - remove brackets if content is empty
     * @return string - output string
     */
    protected static function appendBrackets(string $bracketContent, bool $curved = false, bool $removeEmpty = true): string {

        if ($removeEmpty && empty($bracketContent)) {
            return '';
        }
        return $curved ? '{' . $bracketContent . '}' : "({$bracketContent})";
    }

    /**
     * Get which parameters we want to select
     * Some parameters have upper first -> fix this
     * @param BaseGraphQLEntity $resource
     * @return array - array of selecting ones
     */
    protected function getSelectingParameters(BaseGraphQLEntity $resource): array {

        $objParameters = $resource->getObjectProps();
        $selectParameters = array_filter($objParameters, self::getCalledSelected(...), ARRAY_FILTER_USE_KEY);
        $justSelected = array_filter($selectParameters, static function ($key) use ($objParameters) {
            return $objParameters[$key] === true;
        }, ARRAY_FILTER_USE_KEY);
        $parsed = array_map(self::removeSelectedPrefixAndCamelCase(...), array_keys($justSelected));
        return array_map(static function (string $parameterName) use ($objParameters) {

            if (array_key_exists(ucfirst($parameterName), $objParameters)) {
                return ucfirst($parameterName);
            }
            return $parameterName;
        }, $parsed);
    }

    /**
     * Get the filtering parameters
     * @param BaseGraphQLEntity $resource
     * @return array - filtering parameters array
     */
    protected function getFilteringParameters(BaseGraphQLEntity $resource): array {

        $objParameters = $resource->getObjectProps();
        return array_filter($objParameters, static function($filterParameterValue, string $filterParameterName) use ($resource) {

            if (str_contains($filterParameterName, self::selectedParameterPrefix)
                || str_contains($filterParameterName, self::forcedParameterPrefix)) {
                return false;
            }

            $forceNullable = $resource->parameterIsForcedToBeNullable($filterParameterName);
            return $forceNullable || $filterParameterValue !== null;

        }, ARRAY_FILTER_USE_BOTH);
    }

    /**
     * Check the filtering parameters
     * @param BaseGraphQLEntity $resource
     * @param bool $disableNulls - don't allow null to be parsed (except forced ones)
     * @throws GraphQLCodeException
     * @throws GraphQLCodeException
     * @throws GraphQLCodeException
     * @throws GraphQLSimpleCodeException
     * @todo mby can be changed as global to check if required not null sending
     */
    protected function checkFilteringParameters(BaseGraphQLEntity $resource, bool $disableNulls): void {

        // We need to check also sub-objects
        $objParameters = $resource->getObjectProps();
        foreach ($objParameters as $filterParameterName => $filterParameterValue) {

            if (is_subclass_of($filterParameterValue, BaseGraphQLEntity::class)) {
                $this->checkFilteringParameters($filterParameterValue, $disableNulls);
                continue;
            }

            if (str_contains($filterParameterName, self::selectedParameterPrefix)
                || str_contains($filterParameterName, self::forcedParameterPrefix)) {
                continue;
            }

            if ($resource->parameterIsForcedToBeNullable($filterParameterName)) { continue; }
            if (!$resource->isEntity()) { continue; }

            if ($filterParameterValue === null && $disableNulls) {
                $resourceClass = get_class($resource);
                throw new GraphQLSimpleCodeException("Flag isModifyRequest set but {$resourceClass}::{$filterParameterName} is null! "
                    . "This check should be set if API can't recognize null/undefined values (delete or keep value). "
                    . 'Then you need to send all values or use forced param setter to say to accept null on concrete DTO parameter.');
            }
        }
    }

    /**
     * @param string $resourceClass
     * @param string $selectingParameter
     * @return string|null
     * @throws GraphQLSimpleCodeException
     */
    protected function getPropertyType(string $resourceClass, string $selectingParameter): ?string {

        try {
            $reflectionClass = new ReflectionClass($resourceClass);
            $reflectionProperty = $reflectionClass->getProperty($selectingParameter);
            $reflectionType = $reflectionProperty->getType();
            $parameterType = $reflectionType !== null && method_exists($reflectionType, 'getName') ? $reflectionType->getName() : null;
        } catch (ReflectionException $e) {
            throw new GraphQLSimpleCodeException(message: "Reflection exception on {$resourceClass} and {$selectingParameter}!", previous: $e);
        }

        return $parameterType;
    }

    /**
     * @param string $resourceClass
     * @return string[]
     * @throws ReflectionException
     */
    protected function getClassBaseParameters(string $resourceClass): array {

        $output = [];
        $reflectionClass = new ReflectionClass($resourceClass);
        $reflectionProps = $reflectionClass->getProperties(ReflectionProperty::IS_PUBLIC);
        foreach ($reflectionProps as $prop) {
            if (in_array($prop->getName(), ['usedNameInQuery', 'usedNameInResult', 'usedClassInList'], true)) {
                continue;
            }
            $output[] = $prop->getName();
        }
        return $output;
    }

    /**
     * Check if given class contains needle in DOC comment
     * @param string $className
     * @param string $needle
     * @return bool
     * @throws GraphQLSimpleCodeException
     */
    protected function checkGivenClassDocContainsString(string $className, string $needle): bool {

        try {
            $reflect = new ReflectionClass($className);
            $entityDocComment = $reflect->getDocComment();
            if ($entityDocComment === false) {
                throw new GraphQLSimpleCodeException('Collection class ' . $this::class . ' with missing doc');
            } return str_contains($entityDocComment, $needle);
        } catch (ReflectionException $e) {
            throw new GraphQLSimpleCodeException(message: 'Collection class ' . $this::class . ' with missing doc', previous: $e);
        }
    }
}