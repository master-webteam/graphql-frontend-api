<?php

declare(strict_types=1);

namespace GraphQLFrontApi;

use Contributte\Translation\Translator;
use Nette\Application\UI\Presenter;
use Nette\DI\Attributes\Inject;

/**
 * Class BaseNettePresenter
 * @package GraphQLFrontApi
 */
class BaseNettePresenter extends Presenter {

    #[Inject] public Translator $translator;
}