<?php

declare(strict_types=1);

namespace GraphQLFrontApi;

use AllowDynamicProperties;
use Exception;
use GraphQLFrontApi\Entities\GraphQLDate;
use GraphQLFrontApi\Entities\GraphQLDateTime;
use GraphQLFrontApi\Exceptions\GraphQLCodeException;
use GraphQLFrontApi\Exceptions\GraphQLSimpleCodeException;
use JsonException;
use MyCLabs\Enum\Enum;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;

/**
 * Class BaseGraphQLEntity
 * @package GraphQLFrontApi
 */
#[AllowDynamicProperties] abstract class BaseGraphQLEntity extends BaseGraphQL {

    // Used name in query creator
    public string $usedNameInQuery;

    // Used name while parsing result
    public string $usedNameInResult;

    // Used class in list (return is array)
    public ?string $usedClassInList;

    /** @throws GraphQLSimpleCodeException */
    public function __construct() {

        $this->initProps();
        $this->setUsedNameInQuery($this->getUsedName());
        $this->setUsedNameInResult($this->getUsedName());
        if ($this->isCollection()) { $this->setUsedClassInList($this->getUsedClass()); }
    }

    /**
     * Used for array uniq
     * @return string
     * @throws JsonException
     */
    public function __toString(): string {
        return property_exists($this, 'id')
            ? (string) $this->id
            : json_encode($this, JSON_THROW_ON_ERROR);
    }

    /**
     * Get used class in result from reflection on array entity list param
     * @return string|null
     * @throws GraphQLSimpleCodeException
     */
    public function getUsedClass(): ?string {

        try {
            $reflect = new ReflectionClass($this);
            $property = $reflect->getProperty('list');
        } catch (ReflectionException $e) {
            throw new GraphQLSimpleCodeException(message: 'Class marked as collection but without list for ' . $this::class, previous: $e);
        }

        $propertyDoc = $property->getDocComment();
        if (!$propertyDoc) {
            throw new GraphQLSimpleCodeException(message: 'Collection class ' . $this::class . ' with missing doc on list param');
        }

        return substr($propertyDoc, 9, -5);
    }

    /**
     * Get name used in creator and result parser
     * @return string
     * @throws GraphQLSimpleCodeException
     */
    private function getUsedName(): string {

        $reflect = new ReflectionClass($this);
        $shortName = $reflect->getShortName();
        if ($this->isFunction()) {
            $shortName = substr($shortName, 1);
        } else if ($this->isEntity()) {
            $shortName = substr($shortName, 0, -3);
        }
        return self::camelCase($shortName);
    }

    /**
     * Function to check if current class contains string in DOC commend
     * @param string $needle - string to search
     * @return bool
     * @throws GraphQLSimpleCodeException
     */
    private function checkClassDocContainsString(string $needle): bool {

        $reflect = new ReflectionClass($this);
        $entityDocComment = $reflect->getDocComment();
        if (!$entityDocComment) {
            throw new GraphQLSimpleCodeException('Collection class ' . $this::class . ' with missing doc');
        }

        return str_contains($entityDocComment, $needle);
    }

    /**
     * Class is function class
     * @return bool
     * @throws GraphQLSimpleCodeException
     */
    public function isFunction(): bool { return $this->checkClassDocContainsString('@description function'); }

    /**
     * Class is basic entity
     * @return bool
     * @throws GraphQLSimpleCodeException
     */
    public function isEntity(): bool { return $this->checkClassDocContainsString('@description entity'); }

    /**
     * Class is collection
     * @return bool
     * @throws GraphQLSimpleCodeException
     */
    public function isCollection(): bool { return $this->checkClassDocContainsString('@description collection'); }

    /**
     * Construct object from the result
     * @param array $inputArrayFromJson - input array with json decoded data from API
     * @throws GraphQLCodeException
     * @throws GraphQLSimpleCodeException
     * @noinspection PhpUnused
     */
    public function constructFromInputArray(array $inputArrayFromJson): void {

        // Try to find root in the result
        if (!array_key_exists($this->usedNameInResult, $inputArrayFromJson)) {
            throw new GraphQLSimpleCodeException(message: "Key {$this->usedNameInResult} not found in result array. Forgotten setUsedNameInResult()?");
        }

        // Process all parameters
        $resourceClass = $this::class;
        $input = $inputArrayFromJson[$this->usedNameInResult];
        foreach ($this->getEntityParameters() as $parameter) {

            // If list -> this is fake, if not in result -> null
            if ($parameter === 'list') { continue; }
            if (!isset($input[$parameter])) { continue; }

            try {
                $reflectionClass = new ReflectionClass($resourceClass);
                $reflectionProperty = $reflectionClass->getProperty($parameter);
                $reflectionType = $reflectionProperty->getType();
                $parameterType = $reflectionType !== null && method_exists($reflectionType, 'getName') ? $reflectionType->getName() : null;
            } catch (ReflectionException $e) {
                throw new GraphQLSimpleCodeException(message: "Reflection exception on {$resourceClass} and {$parameter}!", previous: $e);
            }

            // GraphQLDateTime
            if ($parameterType === GraphQLDateTime::class) {

                $foundDot = strpos($input[$parameter], '.');
                $dateValueString = ($foundDot) ? substr($input[$parameter], 0, $foundDot) : $input[$parameter];
                $dateTimeObject = GraphQLDateTime::createFromFormat('Y-m-d\TH:i:s', $dateValueString);
                $dateTimeObjectAlter = GraphQLDateTime::createFromFormat('Y-m-d\TH:i', $dateValueString);

                try {
                    $dateTimeObjectConverted = $dateTimeObject === false ? null : GraphQLDateTime::convertToGraphQLDate($dateTimeObject);
                    $dateTimeObjectAlterConverted = $dateTimeObjectAlter === false ? null : GraphQLDateTime::convertToGraphQLDate($dateTimeObjectAlter);
                } catch (Exception $e) {
                    throw new GraphQLSimpleCodeException(message: "Datetime exception on {$resourceClass} and {$parameter}!", previous: $e);
                }

                $converted = $dateTimeObjectConverted ?? $dateTimeObjectAlterConverted;
                if ($converted !== null) { $this->{$parameter} = $converted; }
                continue;
            }

            // GraphQLDate
            if ($parameterType === GraphQLDate::class) {

                $dateValueString = $input[$parameter];
                $dateTimeObjectRaw = GraphQLDate::createFromFormat('Y-m-d', $dateValueString);
                if ($dateTimeObjectRaw === false) { continue; }

                try { $dateTimeObject = GraphQLDate::convertToGraphQLDate($dateTimeObjectRaw); } catch (Exception $e) {
                    throw new GraphQLSimpleCodeException("Can't convert date for param {$parameter}", $e->getPrevious());
                }

                $this->{$parameter} = $dateTimeObject;
                continue;
            }

            // Enum
            if (is_subclass_of($parameterType, Enum::class)) {
                $this->{$parameter} = new $parameterType($input[$parameter]);
                continue;
            }

            // List
            if (is_subclass_of($parameterType, __CLASS__) && new $parameterType()->usedClassInList !== null) {
                $tmpObject = new $parameterType();
                $inputArray = [$tmpObject->usedNameInResult => $input[$parameter]];
                $tmpObject->constructCollectionFromInputArray($inputArray, $tmpObject->usedClassInList);
                $tmpObject->cleanupResultObject();
                $this->{$parameter} = $tmpObject;
                continue;
            }

            // Object
            if (is_subclass_of($parameterType, __CLASS__)) {
                $tmpObject = new $parameterType();
                $tmpObject->constructFromInputArray([$tmpObject->usedNameInResult => $input[$parameter]]);
                $tmpObject->cleanupResultObject();
                $this->{$parameter} = $tmpObject;
                continue;
            }

            // String
            if (is_string($input[$parameter])) {
                if (strlen($input[$parameter]) > 0) {
                    $this->{$parameter} = str_replace(['\n'], ["\n"], $input[$parameter]);
                }
                continue;
            }

            // Another
            $this->{$parameter} = $input[$parameter];
        }

        // Clean
        $this->cleanupResultObject();
    }

    /**
     * Init the collection from the input array from API
     * @param array $inputCollectionArrayFromJson - input array from API result
     * @param string $classToInit - class name in the collection, with namespace
     * @throws GraphQLCodeException
     * @throws GraphQLSimpleCodeException
     * @throws GraphQLSimpleCodeException
     * @throws GraphQLSimpleCodeException
     * @noinspection PhpUnused
     */
    public function constructCollectionFromInputArray(array $inputCollectionArrayFromJson, string $classToInit): void {

        $collectionParameters = $this->getEntityParameters();
        if (count($collectionParameters) !== 1) {
            throw new GraphQLSimpleCodeException('Count of parameters in collection should be 1');
        }

        $collectionParameter = current($collectionParameters);
        $this->{$collectionParameter} = $this->{$collectionParameter} ?? [];
        if (!isset($inputCollectionArrayFromJson[$this->usedNameInResult])) {
            throw new GraphQLSimpleCodeException("Key {$this->usedNameInResult} not found in result array. Forgotten setUsedNameInResult()?");
        }

        foreach ($inputCollectionArrayFromJson[$this->usedNameInResult] as $item) {

            // We have simple enum list
            if (self::checkGivenClassDocContainsString($classToInit, '@description enum')) {
                $this->{$collectionParameter}[] = new $classToInit($item);
                continue;
            }

            $createdObject = new $classToInit();
            $createdObject->constructFromInputArray([$createdObject->usedNameInResult => $item]);
            $createdObject->cleanupResultObject();
            $this->{$collectionParameter}[] = $createdObject;
        }

        // Clean
        $this->cleanupResultObject();
    }

    /**
     * Just setter for the name used in the query
     * @param string $usedNameInQuery
     * @noinspection PhpUnused
     */
    public function setUsedNameInQuery(string $usedNameInQuery): void { $this->usedNameInQuery = $usedNameInQuery; }

    /**
     * Just setter for the name used in the result
     * @param string $usedNameInResult
     * @noinspection PhpUnused
     */
    public function setUsedNameInResult(string $usedNameInResult): void { $this->usedNameInResult = $usedNameInResult; }

    /**
     * Just setter for the name used in the result
     * @param string $usedClassInList
     * @noinspection PhpUnused
     */
    public function setUsedClassInList(string $usedClassInList): void { $this->usedClassInList = $usedClassInList; }

    /**
     * Check if send also null values to server
     * @param string $propertyName - property name
     * @return bool - true if forced
     */
    public function parameterIsForcedToBeNullable(string $propertyName): bool {

        $forcedPropertyName = self::camelCase(self::forcedParameterPrefix . '-' . $propertyName);
        return $this->{$forcedPropertyName};
    }

    /**
     * Get prop without trash
     * @return array - list of the props with the values
     */
    public function getObjectProps(): array {

        $output = [];
        $reflect = new ReflectionClass($this);
        $objectProperties = get_object_vars($this);

        $reflectProperties = $reflect->getProperties();
        $propsNameList =
            array_filter(
                array_unique(
                    array_merge(
                        array_keys($objectProperties),
                            array_map(static function(ReflectionProperty $property) { return $property->getName(); }, $reflectProperties))),
                static function(string $propName) {
                    return !in_array($propName, ['usedNameInQuery', 'usedNameInResult', 'usedClassInList'], true);
                });

        foreach ($propsNameList as $propName) {
            $output[$propName] = $objectProperties[$propName] ?? null;
        }

        return $output;
    }

    /**
     * Generate functions for use and select
     * @param $func - calling function name
     * @param $params - array of params
     * @return $this|null - current object or null
     */
    public function __call(string $func, array $params): ?object {

        // Find selected
        $objectProperties = $this->getObjectProps();

        // Find select params
        $selectParameters = array_keys(array_filter($objectProperties, static function (string $objectParameter) {
            return str_contains($objectParameter, self::selectedParameterPrefix);
        }, ARRAY_FILTER_USE_KEY));

        // Find base params
        $baseParameters = array_keys(array_filter($objectProperties, static function (string $objectParameter) {
            return !str_contains($objectParameter, self::selectedParameterPrefix) && !str_contains($objectParameter, self::forcedParameterPrefix);
        }, ARRAY_FILTER_USE_KEY));

        // If selected set to true
        if (in_array($func, $selectParameters, true)) {
            $this->{$func} = true;
            return $this;
        }

        // Use magic functionality
        if (str_contains($func, 'use')) {
            $nameWithoutUse = self::camelCase(substr($func, 3));
            if (in_array($nameWithoutUse, $baseParameters, true)) {
                $forcedPropertyName = self::camelCase(self::forcedParameterPrefix . '-' . $nameWithoutUse);
                $this->{$forcedPropertyName} = $params[1] ?? false;
                $this->{$nameWithoutUse} = current($params);
                return $this;
            }
        }
        return null;
    }

    /** Init the props with the null and selects with the false
     * @noinspection PhpUnused
     */
    protected function initProps(): void {

        $this->usedClassInList = null;
        $objectProperties = $this->getObjectProps();
        array_walk($objectProperties, function ($value, $key) {
            $selectPropertyName = self::camelCase(self::selectedParameterPrefix . '-' . $key);
            $this->{$selectPropertyName} = false;
            $forcedPropertyName = self::camelCase(self::forcedParameterPrefix . '-' . $key);
            $this->{$forcedPropertyName} = false;
        });
    }

    /**
     * Get just raw parameters
     * @return array - array of selecting ones
     */
    protected function getEntityParameters(): array {

        $objParameters = $this->getObjectProps();
        $selectParameters = array_filter($objParameters, self::getCalledSelected(...), ARRAY_FILTER_USE_KEY);
        return array_map(self::removeSelectedPrefixAndCamelCase(...), array_keys($selectParameters));
    }

    /**
     * Get output as result array
     * @return array
     * @noinspection PhpUnused
     */
    public function getResultAsArray(): array {

        $outputArray = [];
        foreach ($this->getEntityParameters() as $parameter) {
            $outputArray[$parameter] = $this->{$parameter};
        }
        return $outputArray;
    }

    /** Prepare object for View layer */
    public function cleanupResultObject(): void {

        $objParameters = $this->getObjectProps();
        $selectParameters = array_filter($objParameters, self::getCalledSelected(...), ARRAY_FILTER_USE_KEY);
        array_walk($selectParameters, function ($value, $key) { unset($this->{$key}); });
        $forcedParameters = array_filter($objParameters, self::getCalledForced(...), ARRAY_FILTER_USE_KEY);
        array_walk($forcedParameters, function ($value, $key) { unset($this->{$key}); });
        unset($this->usedNameInQuery, $this->usedNameInResult, $this->usedClassInList);
    }

    /**
     * Select all object parameters in the producing object
     * @throws GraphQLCodeException
     * @throws GraphQLSimpleCodeException
     * @noinspection PhpUnused
     */
    public function selectAll(int $maxDepth, ?array $maxDepthPerProperty = null, ?array $forceWhiteList = null, int $currentDepth = 0): self {

        try {
            $maxDepthPerProperty = $maxDepthPerProperty ?? [];
            if ($forceWhiteList !== null && count($forceWhiteList) > 0) { $maxDepth = 100; }
            $this->selectAllInResourceEntity($maxDepth, $maxDepthPerProperty, $this, $forceWhiteList, $currentDepth);
        } catch (ReflectionException $e) {
            throw new GraphQLSimpleCodeException('Reflection error when selecting all in ' . $this::class, previous: $e);
        }
        return $this;
    }

    /**
     * @throws ReflectionException
     * @throws GraphQLCodeException
     * @throws GraphQLSimpleCodeException
     * @throws GraphQLSimpleCodeException
     */
    private function selectAllInResourceEntity(int $maxDepth, array $maxDepthPerProperty, self $entity, ?array $forceWhiteList = null, int $currentDepth = 0): void {

        // Set all to true (don't select at all the depth is 0 or whitelist used but not found)
        $objParameters = $this->getObjectProps();
        $entityClass = $entity::class;
        $selectParameters = array_filter($objParameters, self::getCalledSelected(...), ARRAY_FILTER_USE_KEY);
        array_walk($selectParameters, static function (&$x, $key) use ($entity, $forceWhiteList, $maxDepthPerProperty, $entityClass, $maxDepth) {

            $keyWithoutSelect = lcfirst(substr($key, 6));
            $keyWithPath = "\\{$entityClass}::{$keyWithoutSelect}";
            $isWhiteListed = $forceWhiteList === null || in_array($keyWithPath, $forceWhiteList, true);
            $currentMaxDepthForProp = $maxDepthPerProperty[$keyWithPath] ?? $maxDepth;
            $isInMaxDepthWithZero = $currentMaxDepthForProp === 0;
            $entity->{$key} = $isWhiteListed && !$isInMaxDepthWithZero;
        });

        // Perform sub-selections
        $baseParameters = $this->getClassBaseParameters($entity::class);
        foreach ($baseParameters as $parameterName) {

            // Get recursion max for this parameter and require white list if depth is 0
            $keyWithPath = "\\{$entityClass}::{$parameterName}";
            $currentMaxDepthForProp = $maxDepthPerProperty[$keyWithPath] ?? $maxDepth;
            $isWhiteListed = $forceWhiteList === null || in_array($keyWithPath, $forceWhiteList, true);

            // Parse
            $propertyType = $this->getPropertyType($entity::class, $parameterName);
            if (is_subclass_of($propertyType, __CLASS__)) {
                $entity->{$parameterName} = new $propertyType();
                if ($entity->{$parameterName}->isCollection()) {
                    if (isset($entity->{$parameterName}->list)) {
                        $isEnum = self::checkGivenClassDocContainsString($entity->{$parameterName}->getUsedClass(), '@description enum');
                        if (!$isEnum) {
                            $entity->{$parameterName}->list = [new ($entity->{$parameterName}->getUsedClass())];
                            if ($isWhiteListed && ($currentDepth < $currentMaxDepthForProp)) {
                                $entity->{$parameterName}->list[0]->selectAll($currentMaxDepthForProp, $maxDepthPerProperty, $forceWhiteList, $currentDepth + 1);
                            }
                        }
                    }
                }
                if ($isWhiteListed && ($currentDepth < $currentMaxDepthForProp)) {
                    $entity->{$parameterName}->selectAll($currentMaxDepthForProp, $maxDepthPerProperty, $forceWhiteList, $currentDepth + 1);
                }
            }
        }
    }
}