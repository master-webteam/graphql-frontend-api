<?php

declare(strict_types=1);

namespace GraphQLFrontApi;

use GraphQLFrontApi\Communication\Entities\SendGraphQLRequestParameters;
use GraphQLFrontApi\Communication\FrontHandlers\AFrontHandler;
use GraphQLFrontApi\Communication\GraphQLCommunication;
use GraphQLFrontApi\Exceptions\Communication\GraphQLApiException;
use GraphQLFrontApi\Exceptions\Communication\GraphQLApiInternalException;
use GraphQLFrontApi\Exceptions\Communication\GraphQLApiNotFoundException;
use GraphQLFrontApi\Exceptions\Communication\GraphQLApiUnauthorizedException;
use GraphQLFrontApi\Exceptions\GraphQLCodeException;
use GraphQLFrontApi\Exceptions\GraphQLSimpleCodeException;
use GraphQLFrontApi\Generators\GenerateRepositories;
use Nette\Application\AbortException;
use ReflectionClass;
use ReflectionException;

/**
 * Class BaseGraphQLRepository
 * @package GraphQLFrontApi
 */
abstract class BaseGraphQLRepository extends BaseGraphQL {

    private const string RETURN_COMMENT_REGEX = '/@return\s(.*)$/m';

    public function __construct(
        protected GraphQLCommunication $graphQLCommunication,
        protected GraphQLCreator $graphQLCreator,
    ) {}

    /** @noinspection PhpUnused */
    public function setFeHandler(AFrontHandler $frontHandler): void {
        $this->graphQLCommunication->setFrontHandler($frontHandler);
    }

    /** @noinspection PhpUnused */
    public function setAuthorization(string $userId, string $token): void {

        $this->graphQLCommunication->userId = $userId;
        $this->graphQLCommunication->bearerToken = $token;
    }

    /** @noinspection PhpUnused */
    public function setScriptMode(?string $userId = null, ?string $token = null): void {
        $this->graphQLCommunication->setScriptMode($userId, $token);
    }

    /** @noinspection PhpUnused */
    public function setTestMode(?string $userId = null, ?string $token = null): void {
        $this->graphQLCommunication->setTestMode($userId, $token);
    }

    /** @noinspection PhpUnused */
    public function getApiDomain(): string {

        return $this->graphQLCommunication->getApiAddress();
    }

    /**
     * @param array $queries
     * @param bool $isMutation
     * @param string $nameSpace
     * @param SendGraphQLRequestParameters|null $parameters
     * @return array
     * @throws AbortException
     * @throws GraphQLApiNotFoundException
     * @throws GraphQLApiInternalException
     * @throws GraphQLApiUnauthorizedException
     * @throws GraphQLApiException
     * @throws GraphQLCodeException
     * @throws GraphQLSimpleCodeException
     * @throws GraphQLSimpleCodeException
     * @throws GraphQLSimpleCodeException
     * @throws GraphQLSimpleCodeException
     * @noinspection PhpUnused
     */
    public function processMultiQuery(array $queries, bool $isMutation, string $nameSpace, ?SendGraphQLRequestParameters $parameters = null): array {

        // Create multi query
        $gqlOutput = $this->graphQLCreator->encapsulateQueries($queries, $isMutation);

        // Send request
        if ($parameters === null) { $parameters = new SendGraphQLRequestParameters(); }
        $parameters->query = $gqlOutput->resultQuery;
        $parameters->validSeconds = $parameters->validSeconds ?? null;
        $resultArray = $this->graphQLCommunication->sendRequestGraphQuery($parameters);

        // Parse result list
        $outputResult = [];
        foreach ($gqlOutput->queryNameList as $queryName) {

            // Fill output with data
            if (array_key_exists($queryName, $resultArray) && $resultArray[$queryName] !== null) {

                $functionClass = $nameSpace . '\\' . ($isMutation ? 'M' : 'Q') . ucfirst(Utils::removeIndexFromQueryName(queryName: $queryName));
                try { $reflection = new ReflectionClass($functionClass); } catch (ReflectionException $e) {
                    throw new GraphQLSimpleCodeException(message: 'Class ' . $functionClass . ' reflection failed: ' . $e->getMessage());
                }
                $doc = $reflection->getDocComment();
                if (!$doc) {
                    throw new GraphQLSimpleCodeException(message: 'Class ' . $functionClass . ' has empty doc');
                }

                preg_match(self::RETURN_COMMENT_REGEX, $doc, $matchesDoc);
                if (!isset($matchesDoc[1])) {
                    throw new GraphQLSimpleCodeException(message: "Impossible to find return notation in : {$functionClass}");
                }
                $returnType = trim($matchesDoc[1]);
                $isBasicType = GenerateRepositories::functionTypeIsBasicType($returnType);

                if (!$isBasicType) {
                    $currentOutput = new $returnType();
                    if ($currentOutput->isCollection()) {
                        $currentOutput->setUsedNameInResult($queryName);
                        $currentOutput->constructCollectionFromInputArray($resultArray, $currentOutput->getUsedClass());
                    } else {
                        $currentOutput->setUsedNameInResult($queryName);
                        $currentOutput->constructFromInputArray($resultArray);
                    }
                    $outputResult[$queryName] = $currentOutput;
                } else {
                    $outputResult[$queryName] = $resultArray[$queryName];
                }
            }
        }

        return $outputResult;
    }
}