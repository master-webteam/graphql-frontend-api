<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Generators;

use GraphQLFrontApi\BaseGraphQLEntity;
use GraphQLFrontApi\Exceptions\GraphQLSimpleCodeException;
use GraphQLFrontApi\Generators\Entities\GeneratorEntity;
use GraphQLFrontApi\Generators\Entities\GeneratorEntityParameter;
use GraphQLFrontApi\Generators\Entities\Schema;
use GraphQLFrontApi\Utils;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;

/**
 * Class GenerateFunctions
 * @package GraphQLFrontApi\Generators
 */
class GenerateFunctions {

    /** @var GeneratorEntity[] */
    public array $functions = [];

    private const string functionRegex = '/(\"([\s\S]+?)\")?\s*([a-zA-Z]+)(\([\s\S]+?\))?:\s*([a-zA-Z0-9\[\]_!]+)/mx';

    private const string functionParametersRegex = '/([a-zA-Z0-9]+)\s*:\s*([\[!a-zA-Z0-9\]]+)\s*=?\s*([a-zA-Z]*)/';

    private const string queryRegex = '/type\s+Query\s+{([\s\S]+?)}/m';

    private const string mutationRegex = '/type\s+Mutation\s+{([\s\S]+?)}/m';

    private const string commentRegex = '/\"([\s\S]+?)\"/';

    public function __construct(
        private readonly string $entitiesNameSpace,
        private readonly string $entitiesDir,
        private readonly Schema $schema) {
    }

    /**
     * @throws GraphQLSimpleCodeException
     */
    public function parseFunctions(): void {

        preg_match_all(self::queryRegex, GeneratorUtil::loadScheme($this->schema), $queryContent, PREG_SET_ORDER);
        if (!isset($queryContent[0][1])) {
            Utils::reportWarning('Empty queries or impossible to parse them');
        } else {
            $this->parseFunctionsByType('Q', $queryContent[0][1]);
        }

        preg_match_all(self::mutationRegex, GeneratorUtil::loadScheme($this->schema), $queryContent, PREG_SET_ORDER);
        if (!isset($queryContent[0][1])) {
            Utils::reportWarning('Empty mutations or impossible to parse them');
        } else {
            $this->parseFunctionsByType('M', $queryContent[0][1]);
        }
    }

    /**
     * @param string $type
     * @param string $content
     * @throws GraphQLSimpleCodeException
     */
    public function parseFunctionsByType(string $type, string $content): void {

        preg_match_all(self::functionRegex, trim($content), $queryFunctions, PREG_SET_ORDER);
        foreach ($queryFunctions as $item) {

            // Comment
            $function = new GeneratorEntity();
            if (!empty($item[2])) {
                $function->comment = trim($item[2]);
            }

            // Name
            if (!isset($item[3])) {
                throw new GraphQLSimpleCodeException(message: "Can't parse function name!");
            }
            $function->name = trim($item[3]);

            // Types
            $function->functionType = $type;
            if (!isset($item[5])) {
                throw new GraphQLSimpleCodeException(message: "Can't parse function return type!");
            }

            $function->functionReturnTypeRequired = str_contains($item[5], '!');
            $function->functionReturnType = GeneratorUtil::convertTypes(type: (trim($item[5])), nameSpace: $this->entitiesNameSpace);
            $function->functionReturnTypeSimple = GeneratorUtil::convertTypes(trim($item[5]), nameSpace: $this->entitiesNameSpace, simpleArrays: true);

            // Parameters
            $function->parameters = [];
            if (!isset($item[4])) {
                throw new GraphQLSimpleCodeException(message: "Can't parse function parameters!");
            }
            $explodedParameters = empty($item[4]) ? [] : explode(',', substr($item[4], 1, -1));
            foreach ($explodedParameters as $functionParameter) {

                // Parameter comment
                $parameterName = trim($functionParameter);
                $parameter = new GeneratorEntityParameter();
                preg_match_all(self::commentRegex, $parameterName, $matches, PREG_SET_ORDER);
                if (isset($matches[0][1])) {
                    $parameter->comment = $matches[0][1];
                    $parameterName = trim(preg_replace(self::commentRegex, '', $parameterName));
                }

                // Parameter name and type
                preg_match_all(self::functionParametersRegex, $parameterName, $matches, PREG_SET_ORDER);

                // Name
                if (!isset($matches[0][1])) {
                    throw new GraphQLSimpleCodeException(message: "Can't parse function parameter name for {$function->name}!");
                }
                $parameter->name = trim($matches[0][1]);

                // Type
                if (!isset($matches[0][2])) {
                    throw new GraphQLSimpleCodeException(message: "Can't parse function parameter type for {$function->name}!");
                }
                $typeValue = trim($matches[0][2]);
                $parameter->type = GeneratorUtil::convertTypes(type: $typeValue, nameSpace: $this->entitiesNameSpace);
                $parameter->simpleType = GeneratorUtil::convertTypes($typeValue, $this->entitiesNameSpace, true);
                $parameter->defaultValue = GeneratorUtil::decodeParameterDefaultValue($matches[0][3], $parameter->type);
                $parameter->required = preg_match('/!/', $typeValue) === 1;
                $function->parameters[] = $parameter;
            }

            usort($function->parameters, static function(GeneratorEntityParameter $a, GeneratorEntityParameter $b) {
                if ($a->required === true && ($b->required === false || $b->required === null)) { return -1; }
                if (($a->required === false || $a->required === null) && $b->required === true) { return 1; }
                return 0;
            });

            $this->functions[] = $function;
        }
    }

    /**
     * @throws GraphQLSimpleCodeException
     */
    public function saveFunctions(): void {

        $mutationsDir = "{$this->entitiesDir}/Mutations";
        GeneratorUtil::createForceDir($mutationsDir);
        $queriesDir = "{$this->entitiesDir}/Queries";
        GeneratorUtil::createForceDir($queriesDir);

        foreach ($this->functions as $function) {

            $file = new PhpFile();
            $file->addComment(GeneratorUtil::getIgnoredComment());
            $file->setStrictTypes();
            $namespace = $file->addNamespace($this->entitiesNameSpace);
            $functionName = $function->functionType . ucfirst($function->name);

            $createEntity = $namespace->addClass($functionName);
            $createEntity->setFinal();
            $createEntity->setExtends(BaseGraphQLEntity::class);
            $createEntity->addComment("Class {$functionName}");
            $createEntity->addComment('This file is auto generated by GraphQL generator.');
            $createEntity->addComment('@version ' . $this->schema->version);
            $createEntity->addComment('@package ' . $this->entitiesNameSpace);
            $createEntity->addComment('@description function');
            $createEntity->addComment("@return {$function->functionReturnType}");
            if ($function->comment !== null) {
                $createEntity->addComment($function->comment);
            }

            foreach ($function->parameters as $parameter) {

                $parameterTypeComment = $parameter->type;
                if (empty($parameter->defaultValue)) {
                    $parameterTypeComment .= '|null';
                }

                $createEntity->addComment("@method {$functionName} use" . ucfirst($parameter->name) . "({$parameterTypeComment} \${$parameter->name}, bool \$force = false)");
                $property = $createEntity->addProperty($parameter->name, $parameter->defaultValue)
                    ->setVisibility('public')
                    ->setType($parameter->simpleType . (empty($parameter->defaultValue) ? '|null' : ''))
                    ->setInitialized();

                $property->addComment($parameter->required ? '@required' : '@optional');
                if ($parameter->comment !== null) {
                    $property->addComment($parameter->comment);
                }
            }

            $printer = new PsrPrinter();
            $outputDir = $function->functionType === 'M' ? $mutationsDir : $queriesDir;
            file_put_contents("{$outputDir}/{$functionName}.php", $printer->printFile($file));
        }
    }
}