<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Generators;

use GraphQLFrontApi\Entities\GraphQLDate;
use GraphQLFrontApi\Entities\GraphQLDateList;
use GraphQLFrontApi\Entities\GraphQLDateTime;
use GraphQLFrontApi\Exceptions\GraphQLSimpleCodeException;
use GraphQLFrontApi\Generators\Entities\Schema;

/**
 * Class GeneratorUtil
 * @package GraphQLFrontApi\Generators
 */
class GeneratorUtil {

    private const string commentsRemoveRegex = '/^\s*#.*$/m';

    /** @noinspection PhpUnused */
    public static function removeCommaFromComments(array $matches): string {

        return str_replace(',', ';', $matches[0]);
    }

    /**
     * @param Schema $schema
     * @return string
     * @throws GraphQLSimpleCodeException
     */
    public static function loadScheme(Schema $schema): string {

        $schemaContent = file_get_contents("{$schema->path}/{$schema->file}");
        if (!$schemaContent) {
            throw new GraphQLSimpleCodeException(message: "Could not read schema {$schema->file} from {$schema->path}!");
        }

        $schemaContent = preg_replace(self::commentsRemoveRegex, '', $schemaContent);
        $schemaContent = str_replace('@_mappedOperation(operation : "__internal__")', '', $schemaContent);
        $schemaContent = preg_replace_callback("|(\"[\s\S]+?\")|", self::removeCommaFromComments(...), $schemaContent);

        if (!$schemaContent) {
            throw new GraphQLSimpleCodeException(message: "Could not load schema {$schema->file} from {$schema->path}!");
        }
        return $schemaContent;
    }

    /**
     * @param string $path
     * @param bool $alsoDelete
     * @throws GraphQLSimpleCodeException
     */
    public static function createForceDir(string $path, bool $alsoDelete = true): void {

        if ($alsoDelete && file_exists($path)) {
            exec("rm -rf {$path}");
        }
        if (!file_exists($path) && !mkdir(directory: $path, recursive: true) && !is_dir($path)) {
            throw new GraphQLSimpleCodeException(message: sprintf('Directory "%s" was not created', $path));
        }
    }

    public static function getIgnoredComment(): string {

        return '@noinspection PhpUnnecessaryFullyQualifiedNameInspection, PhpFullyQualifiedNameUsageInspection, PhpUnused, ' .
            'SpellCheckingInspection, PhpUnusedParameterInspection, PhpUnusedLocalVariableInspection, RedundantSuppression';
    }

    /**
     * @param string $type
     * @param string|null $nameSpace
     * @param bool $simpleArrays
     * @return string
     */
    public static function convertTypes(string $type, ?string $nameSpace, bool $simpleArrays = false): string {

        $type = str_replace('!', '', $type);

        if ($type === '[String]' && !$simpleArrays) {
            return 'string[]';
        }
        if ($type === '[String]' && $simpleArrays === true) {
            return 'array';
        }
        if ($type === '[Int]' && !$simpleArrays) {
            return 'int[]';
        }
        if ($type === '[Int]' && $simpleArrays === true) {
            return 'array';
        }
        if ($type === '[LocalDate]') {
            return '\\' . GraphQLDateList::class;
        }
        if ($type === 'HashMap_String_IntegerScalar') {
            return 'array';
        }

        if (str_contains($type, '[')) {

            $typeClass = str_replace(['[', ']', '_'], ['', '', ''], $type);
            $name = str_replace('Dto', '', $typeClass) . 'List';
            return $nameSpace !== null ? '\\' . $nameSpace . '\\' . $name : $name;
        }

        switch ($type) {
            case 'BigInteger':
            case 'Long':
            case 'Int' :
                return 'int';
            case 'Base64String':
            case 'String' :
                return 'string';
            case 'Float' :
                return 'float';
            case 'Boolean' :
                return 'bool';
            case 'LocalDateTime' :
                return '\\' . GraphQLDateTime::class;
            case 'LocalDate' :
                return '\\' . GraphQLDate::class;
            default:
                $className = str_replace('_', '', $type);
                return $nameSpace !== null ? '\\' . $nameSpace . '\\' . $className : $className;
        }
    }

    /**
     * @param string $defaultValue
     * @param string $type
     * @return bool|float|int|string|null
     */
    public static function decodeParameterDefaultValue(string $defaultValue, string $type): float|bool|int|string|null {

        if (empty($defaultValue) || $defaultValue === 'null') {
            return null;
        }
        if ($type === 'bool') {
            return $defaultValue === 'true';
        }
        if ($type === 'int') {
            return (int) $defaultValue;
        }
        if ($type === 'float') {
            return (float) $defaultValue;
        }
        return $defaultValue;
    }
}