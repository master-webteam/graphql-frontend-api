<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Generators;

use GraphQLFrontApi\Exceptions\GraphQLSimpleCodeException;
use GraphQLFrontApi\Generators\Entities\Schema;
use GraphQLFrontApi\Utils;

/**
 * Class GraphQLGenerator
 * @package GraphQLFrontApi
 */
class GraphQLGenerator {

    private array $backends;

    private bool $nullableMode;

    /** @noinspection PhpUnused */
    public function setBackends(array $backends): void { $this->backends = $backends; }

    /** @noinspection PhpUnused */
    public function setNullableMode(bool $nullableMode): void { $this->nullableMode = $nullableMode; }

    /**
     * Process all generations for all backends
     * @throws GraphQLSimpleCodeException
     * @noinspection PhpUnused
     */
    public function processEntitiesForAllBackends(): void {

        $index = 0;
        $total = count($this->backends);

        foreach ($this->backends as $backend) {

            Utils::reportInfo('Parsing schema ' . $backend['schemaFile'] . ' version ' . $backend['schemaVersion']);
            $schema = new Schema($backend['schemaVersion'], $backend['schemaFile'], $backend['schemaDir']);

            $enumsGenerator = new GenerateEnums($backend['entitiesNameSpace'], $backend['entitiesDir'], $schema);
            $enumsGenerator->parseEnums();
            $enumsGenerator->saveEnums();
            $count = count($enumsGenerator->enums);
            if ($count > 0) {
                Utils::reportSuccess("Saved {$count} enum entities");
            }

            $entitiesGenerator = new GenerateEntities($backend['entitiesNameSpace'], $backend['entitiesDir'], $schema, $this->nullableMode);
            $entitiesGenerator->parseEntities();
            $entitiesGenerator->saveEntities();
            $count = count($entitiesGenerator->entities);
            if ($count > 0) {
                Utils::reportSuccess("Saved {$count} base entities");
            }

            $collectionsGenerator = new GenerateCollections($backend['entitiesNameSpace'], $backend['entitiesDir'],
                $schema, $entitiesGenerator->entities, $enumsGenerator->enums);
            $collectionsGenerator->saveCollections();
            if ($count > 0) {
                Utils::reportSuccess('Saved collection entities');
            }

            $functionsGenerator = new GenerateFunctions($backend['entitiesNameSpace'], $backend['entitiesDir'],
                $schema);
            $functionsGenerator->parseFunctions();
            $functionsGenerator->saveFunctions();
            $count = count($functionsGenerator->functions);
            if ($count > 0) {
                Utils::reportSuccess("Saved {$count} function entities");
            }

            ++$index;
            Utils::reportFinish("Done {$index}/{$total}");
        }
    }

    /**
     * Process all generations for all backends
     * @throws GraphQLSimpleCodeException
     * @noinspection PhpUnused
     */
    public function processRepositoriesForAllBackends(): void {

        $total = count($this->backends);
        $index = 0;
        foreach ($this->backends as $backend) {

            Utils::reportInfo('Parsing schema ' . $backend['schemaFile'] . ' version ' . $backend['schemaVersion']);
            $schema = new Schema($backend['schemaVersion'], $backend['schemaFile'], $backend['schemaDir']);

            $enumsGenerator = new GenerateEnums($backend['entitiesNameSpace'], $backend['entitiesDir'], $schema);
            $enumsGenerator->parseEnums();
            $count = count($enumsGenerator->enums);
            if ($count > 0) {
                Utils::reportSuccess("Parsed {$count} enum entities");
            }

            $entitiesGenerator = new GenerateEntities($backend['entitiesNameSpace'], $backend['entitiesDir'], $schema, $this->nullableMode);
            $entitiesGenerator->parseEntities();
            $count = count($entitiesGenerator->entities);
            if ($count > 0) {
                Utils::reportSuccess("Parsed {$count} base entities");
            }

            $functionsGenerator = new GenerateFunctions($backend['entitiesNameSpace'], $backend['entitiesDir'], $schema);
            $functionsGenerator->parseFunctions();
            $countFunctions = count($functionsGenerator->functions);
            if ($countFunctions > 0) {
                Utils::reportSuccess("Parsed {$countFunctions} function entities");
            }

            $schema = new Schema($backend['schemaVersion'], $backend['schemaFile'], $backend['schemaDir']);
            $repositoriesGenerator = new GenerateRepositories($functionsGenerator->functions, $schema, $backend['entitiesNameSpace'], $backend['entitiesDir']);
            $repositoriesGenerator->saveRepositories();
            if ($count > 0) {
                Utils::reportSuccess("Saved repositories with {$countFunctions} functions");
            }

            ++$index;
            Utils::reportFinish("Done {$index}/{$total}");
        }
    }
}