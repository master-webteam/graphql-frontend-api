<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Generators\Entities;

/**
 * Class GeneratorEntityParameter
 * @package GraphQLFrontApi\Entities
 */
class GeneratorEntityParameter {

    public ?GeneratorEntityParameterDeprecated $deprecated = null;

    public ?string $comment = null;

    public ?string $name = null;

    public ?string $type = null;

    public ?string $simpleType = null;

    public ?bool $required = null;

    public string|int|bool|null|float $defaultValue = null;
}