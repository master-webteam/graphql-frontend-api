<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Generators\Entities;

class GeneratorEntityParameterDeprecated {

    public function __construct(public ?string $reason) {}
}