<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Generators\Entities;

/**
 * Class SchemaWithURI
 * @package GraphQLFrontApi\Entities
 */
class SchemaWithURI extends Schema {

    public function __construct(
        public string $version,
        public string $file,
        public string $path,
        public string $uri) {
        parent::__construct($version, $file, $path);
    }
}