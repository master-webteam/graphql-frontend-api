<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Generators\Entities;

/**
 * Class Schema
 * @package GraphQLFrontApi\Entities
 */
class Schema {

    public function __construct(
        public string $version,
        public string $file,
        public string $path) {
    }
}