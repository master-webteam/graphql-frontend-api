<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace GraphQLFrontApi\Generators\Entities;

/**
 * Class GeneratorEntity
 * @package GraphQLFrontApi\Entities
 */
class GeneratorEntity {

    public ?string $name = null;

    public ?bool $deprecated = null;

    public ?string $comment = null;

    /** @var GeneratorEntityParameter[] */
    public array $parameters = [];

    public ?string $functionType = null;

    public ?string $functionReturnType = null;

    public ?string $functionReturnTypeSimple = null;

    public ?bool $functionReturnTypeRequired = null;
}