<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Debug;

use GraphQLFrontApi\Communication\Entities\SendFileRequestParameters;
use GraphQLFrontApi\Communication\Entities\SendGraphQLRequestParameters;
use GraphQLFrontApi\Communication\FrontHandlers\InternalFrontHandlerWrapper;
use GraphQLFrontApi\Logger;
use JetBrains\PhpStorm\Pure;
use Nette\Http\Request;

/**
 * Class DebugObject
 * @package GraphQLFrontApi
 */
class DebugObject {

    // Input variable parameters debug
    public SendGraphQLRequestParameters|SendFileRequestParameters $parameters;

    // Request debug
    public ?Request $request = null;

    // CURL debug
    public DebugObjectGuzzle $requestDebug;

    // Class debug
    public DebugObjectCommunicator $communicatorDebug;

    // Cache debug
    public DebugObjectCache $cacheDebug;

    // User debug
    public DebugObjectUser $userDebug;

    // Logger object
    public ?Logger $logger = null;

    // Api errors
    public ?array $errors = null;

    // Custom client side uniq key for checking error codes
    public ?array $virtualUniqErrorKeys = null;

    // Simplified name of endpoint / query
    public ?string $canonizedIdentify = null;

    public ?array $stack = null;

    #[Pure] public function __construct(public ?InternalFrontHandlerWrapper $internalFrontHandler) {

        $this->requestDebug = new DebugObjectGuzzle();
        $this->communicatorDebug = new DebugObjectCommunicator();
        $this->cacheDebug = new DebugObjectCache();
        $this->userDebug = new DebugObjectUser();
    }
}
