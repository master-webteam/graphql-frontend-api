<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Debug;

/**
 * Class DebugObjectCommunicator
 * @package GraphQLFrontApi
 */
class DebugObjectCommunicator {

    public ?string $domain = null;

    public ?bool $debugModeEnabled = null;

    public ?bool $unauthorizedMode = null;

    public ?bool $freshUpdate = null;

    public ?string $userId = null;

    public ?string $languageISO = null;

    public ?string $bearerToken = null;
}
