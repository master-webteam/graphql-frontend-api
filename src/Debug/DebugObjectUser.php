<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Debug;

/**
 * Class DebugObjectUser
 * @package App\Models\Logger
 */
class DebugObjectUser {

    // User token
    public ?string $userToken = null;

    // User data
    public ?array $userData = null;

    // User ID
    public ?string $userId = null;
}
