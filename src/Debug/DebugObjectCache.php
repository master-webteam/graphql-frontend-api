<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Debug;

/**
 * Class DebugObjectCache
 * @package GraphQLFrontApi
 */
class DebugObjectCache {

    // If cached
    public ?bool $cached = null;

    // Cache key
    public ?string $cachedKey = null;

    // Cache result
    public ?string $result = null;

    // Cache tags used
    public ?array $tags = null;
}
