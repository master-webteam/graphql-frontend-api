<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Debug;

use GraphQLFrontApi\Entities\GuzzleHandle;

class DebugObjectGuzzle {

    public ?GuzzleHandle $guzzleHandle = null;

    public ?string $errnoMsg = null;

    public ?float $transferTime = null;

    public ?array $handlerStats = null;
    
    public ?string $responseBody = null;

    /** @var string[][]|null */
    public array|null $responseHeaders = null;

    public ?int $responseHttpCode = null;

    public ?string $responseHttpCodeMessage = null;

    public ?array $resultArray = null;
}
