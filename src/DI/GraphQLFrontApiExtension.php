<?php

declare(strict_types=1);

/** @noinspection PhpUnused */

namespace GraphQLFrontApi\DI;

use GraphQLFrontApi\Communication\FilesCommunication;
use GraphQLFrontApi\Communication\GraphQLCommunication;
use GraphQLFrontApi\Generators\GraphQLGenerator;
use GraphQLFrontApi\GraphQLCreator;
use GraphQLFrontApi\GraphQLParameters;
use GraphQLFrontApi\Logger;
use GraphQLFrontApi\TestCreator;
use Nette\DI\CompilerExtension;

/**
 * Class GraphQLFrontApiExtension
 * @package GraphQLFrontApi\DI
 * @noinspection PhpUnused
 */
class GraphQLFrontApiExtension extends CompilerExtension {

    /**
     * @noinspection PhpUnused
     */
    public function loadConfiguration(): void {

        $config = $this->getConfig();
        $builder = $this->getContainerBuilder();

        $builder->addDefinition($this->prefix('logger'))
            ->setType(Logger::class)
            ->addSetup('setSpikeTimeoutSeconds', [$config['spikeTimeoutSeconds'] ?? null])
            ->addSetup('setIgnored', [$config['ignored'] ?? null])
            ->addSetup('setVersion', [$config['version']])
            ->addSetup('setEnvironment', [$config['environment'] ?? null])
            ->addSetup('setLogDebugMessages', [$config['logDebugMessages'] ?? false])
            ->addSetup('setLocalLogSimplified', [$config['localLogSimplified'] ?? false])
            ->addSetup('setLogPath', [$config['logPath']]);

        foreach ($config['backends'] as $name => $backend) {

            if (isset($backend['filesApiUrl']) && is_string($backend['filesApiUrl'])) {
                $builder->addDefinition($this->prefix("files{$name}"))
                    ->setType(FilesCommunication::class)
                    ->addSetup('setFilesDomain', [$backend['filesApiUrl']])
                    ->addSetup('setUnauthorizedLink', [$backend['unauthorizedLink']])
                    ->addSetup('setLogCallStack', [$config['logCallStack']])
                    ->addSetup('setLoginLink', [$backend['loginLink']])
                    ->addSetup('setDebug', [$backend['debugMode'] ?? true])
                    ->addSetup('setUnauthorizedMode', [$backend['unauthorizedMode'] ?? false]);
            }
            if (isset($backend['graphQLApiUrl']) && is_string($backend['graphQLApiUrl'])){
                $builder->addDefinition($this->prefix("communicator{$name}"))
                    ->setType(GraphQLCommunication::class)
                    ->addSetup('setAutoResponsePayloadUniqCodeMapping', [$config['customAutoResponsePayloadUniqCodeMapping'] ?? null])
                    ->addSetup('setCustomTranslationKeys', [$config['customTranslationKeys'] ?? null])
                    ->addSetup('setCustomApiExceptionsMapping', [$config['customApiExceptionsMapping'] ?? null])
                    ->addSetup('setGraphQLDomain', [$backend['graphQLApiUrl']])
                    ->addSetup('setLogCallStack', [$config['logCallStack']])
                    ->addSetup('setUnauthorizedLink', [$backend['unauthorizedLink']])
                    ->addSetup('setLoginLink', [$backend['loginLink']])
                    ->addSetup('setDebug', [$backend['debugMode'] ?? true])
                    ->addSetup('setDisableCache', [$backend['disableCache'] ?? false])
                    ->addSetup('setPerformanceLoggingEnable', [$backend['performanceLoggingEnable'] ?? false])
                    ->addSetup('setPerformanceLoggingSampleRate', [$backend['performanceLoggingSampleRate'] ?? 1.0])
                    ->addSetup('setPerformanceLoggingFile', [$backend['performanceLoggingFile']])
                    ->addSetup('setUnauthorizedMode', [$backend['unauthorizedMode'] ?? false]);
            }
        }

        $builder->addDefinition($this->prefix('generator'))
            ->setType(GraphQLGenerator::class)
            ->addSetup('setNullableMode', [$config['nullableMode']])
            ->addSetup('setBackends', [$config['backends']]);

        $builder->addDefinition($this->prefix('parameters'))
            ->setType(GraphQLParameters::class)
            ->addSetup('setParameters', [$config['backends']]);

        $builder->addDefinition($this->prefix('testcreator'))->setType(TestCreator::class);
        $builder->addDefinition($this->prefix('creator'))->setType(GraphQLCreator::class);
    }
}