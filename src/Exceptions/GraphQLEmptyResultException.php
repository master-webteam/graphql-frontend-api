<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Exceptions;

/**
 * Class GraphQLEmptyResultException
 * Thrown when empty data returned but some requested
 * @package GraphQLFrontApi
 * @noinspection PhpUnused
 */
class GraphQLEmptyResultException extends GraphQLBaseException {}