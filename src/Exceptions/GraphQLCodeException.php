<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Exceptions;
use Contributte\Translation\Exceptions\InvalidArgument;
use GraphQLFrontApi\Communication\FrontHandlers\ResponsePayload;
use GraphQLFrontApi\Communication\FrontHandlers\ResponsePayloadTypeEnum;
use GraphQLFrontApi\Debug\DebugObject;
use Nette\Application\AbortException;
use Throwable;

class GraphQLCodeException extends GraphQLException {

    public function __construct(string $message, DebugObject $debugObject, ?Throwable $previous = null) {
        parent::__construct(debugObject: $debugObject, message: $message, previous: $previous, code: 503);
    }

    /**
     * @throws GraphQLSimpleCodeException
     * @throws AbortException
     */
    public function autoResponseReport(string $messageTranslationKey, bool $sendNow = true): void {

        try {
            $this->internalFrontHandler->frontHandler->handleApiResponse(
                responsePayload: new ResponsePayload(
                    message: $this->internalFrontHandler->frontHandler->translateKey($messageTranslationKey),
                    type: ResponsePayloadTypeEnum::TYPE_ERROR,
                    closeModal: false,
                    uniqKeys: ['APP', 'CD', $this->callerFunction, $this->callerLine],
                    sentryEventId: $this->sentryEventId
                ),
                sendNow: $sendNow,
            );
        } catch (InvalidArgument $e) {
            throw new GraphQLSimpleCodeException("Error translating key: {$messageTranslationKey}", $e);
        }
    }
}