<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Exceptions;
use Throwable;

class GraphQLSimpleCodeException extends GraphQLBaseException {

    public function __construct(string $message, ?Throwable $previous = null) {
        parent::__construct(message: $message, previous: $previous, code: 503);
    }
}