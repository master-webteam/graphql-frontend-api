<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Exceptions\Communication;

use Contributte\Translation\Exceptions\InvalidArgument;
use GraphQLFrontApi\Communication\FrontHandlers\ResponsePayload;
use GraphQLFrontApi\Communication\FrontHandlers\ResponsePayloadTypeEnum;
use GraphQLFrontApi\Debug\DebugObject;
use GraphQLFrontApi\Exceptions\GraphQLException;
use GraphQLFrontApi\Exceptions\GraphQLSimpleCodeException;
use Nette\Application\AbortException;

/**
 * Class GraphQLCommunicationResponseErrorUnauthorizedException
 * Response for server means any kind of unauthorized
 * @package GraphQLFrontApi\Exceptions
 */
class GraphQLApiUnauthorizedException extends GraphQLException {

    public function __construct(DebugObject $debugObject, ?string $message = null) {
        parent::__construct(debugObject: $debugObject, message: $message, code: 404);
    }

    /**
     * @throws GraphQLSimpleCodeException
     * @throws AbortException
     */
    public function autoResponseReport(string $messageTranslationKey, bool $sendNow = true): void {

        if ($this->internalFrontHandler === null) {
            throw new GraphQLSimpleCodeException('Missing frontend handler for autoResponseReport!');
        }

        try {
            $this->internalFrontHandler->frontHandler->handleApiResponse(
                responsePayload: new ResponsePayload(
                    message: $this->internalFrontHandler->frontHandler->translateKey($messageTranslationKey),
                    type: ResponsePayloadTypeEnum::TYPE_ERROR,
                    closeModal: false,
                    uniqKeys: ['API', 'UA',
                        $this->debugObject->userDebug->userId,
                        $this->debugObject->communicatorDebug->bearerToken !== null ? strlen($this->debugObject->communicatorDebug->bearerToken) : 0],
                    sentryEventId: $this->sentryEventId,
                ),
                sendNow: $sendNow
            );
        } catch (InvalidArgument $e) {
            throw new GraphQLSimpleCodeException("Error translating key: {$messageTranslationKey}", $e);
        }
    }
}