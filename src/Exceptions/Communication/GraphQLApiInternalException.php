<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Exceptions\Communication;

use Contributte\Translation\Exceptions\InvalidArgument;
use GraphQLFrontApi\Communication\FrontHandlers\ResponsePayload;
use GraphQLFrontApi\Communication\FrontHandlers\ResponsePayloadTypeEnum;
use GraphQLFrontApi\Debug\DebugObject;
use GraphQLFrontApi\Entities\InternalSpecialErrorEventEnum;
use GraphQLFrontApi\Exceptions\GraphQLException;
use GraphQLFrontApi\Exceptions\GraphQLSimpleCodeException;
use Nette\Application\AbortException;

/**
 * Class GraphQLCommunicationResponseInternalErrorException
 * Response from server means internal error
 * @package GraphQLFrontApi\Exceptions
 */
class GraphQLApiInternalException extends GraphQLException {

    public function __construct(DebugObject $debugObject, string $message, ?int $code = null) {
        parent::__construct(debugObject: $debugObject, message: $message, code: $code);
    }

    private const string TIMEOUT_ERROR_REGEX = '/Operation timed out after \d+ milliseconds with \d+ bytes received/m';

    /** @noinspection PhpUnused */
    public function timeoutErrorPresented(): bool {

        if ($this->debugObject->requestDebug->errnoMsg === null) { return false; }
        $find = preg_match_all(self::TIMEOUT_ERROR_REGEX, $this->debugObject->requestDebug->errnoMsg, $matches, PREG_SET_ORDER);
        return $find !== false && $find > 0;
    }

    /**
     * @throws GraphQLSimpleCodeException
     * @throws AbortException
     */
    public function autoResponseReport(string $messageTranslationKey, bool $sendNow = true): void {

        if ($this->internalFrontHandler === null) {
            throw new GraphQLSimpleCodeException('Missing frontend handler for autoResponseReport!');
        }

        $specialMessageTranslationKey = $this->convertInternalErrorEventToTranslationMapping();

        try {
            $this->internalFrontHandler->frontHandler->handleApiResponse(
                responsePayload: new ResponsePayload(
                    message: $this->internalFrontHandler->frontHandler->translateKey($specialMessageTranslationKey ?? $messageTranslationKey),
                    type: ResponsePayloadTypeEnum::TYPE_ERROR,
                    closeModal: false,
                    uniqKeys: ['API', 'INT', $this->debugObject->requestDebug->responseHttpCode],
                    sentryEventId: $this->sentryEventId
                ),
                sendNow: $sendNow
            );
        } catch (InvalidArgument $e) {
            throw new GraphQLSimpleCodeException("Error translating key: {$messageTranslationKey}", $e);
        }
    }

    private function convertInternalErrorEventToTranslationMapping(): string|null {

        $mappings = $this->internalFrontHandler->customTranslationKeys;
        if ($mappings !== null) {
            if ($this->timeoutErrorPresented()) {
                return $mappings[InternalSpecialErrorEventEnum::TIMEOUT->value] ?? null;
            }
        }

        return null;
    }
}