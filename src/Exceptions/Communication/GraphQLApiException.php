<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Exceptions\Communication;

use Contributte\Translation\Exceptions\InvalidArgument;
use GraphQLFrontApi\Communication\FrontHandlers\ResponsePayload;
use GraphQLFrontApi\Communication\FrontHandlers\ResponsePayloadTypeEnum;
use GraphQLFrontApi\Debug\DebugObject;
use GraphQLFrontApi\Exceptions\GraphQLException;
use GraphQLFrontApi\Exceptions\GraphQLSimpleCodeException;
use GraphQLFrontApi\Utils;
use JetBrains\PhpStorm\Deprecated;
use Nette\Application\AbortException;

/**
 * Class GraphQLApiException
 * Used for API specific warnings
 * @package GraphQLFrontApi\Exceptions
 */
class GraphQLApiException extends GraphQLException {

    public function __construct(DebugObject $debugObject) {
        parent::__construct(debugObject: $debugObject);
    }

    /** @noinspection PhpUnused */
    public function notExistsErrorPresented(): bool {

        if ($this->errors === null) { return false; }
        return array_any($this->errors, fn($error) => $error->errorType === 'NOT_EXISTS');
    }

    /**
     * Try to find validation uniqueKey
     * @noinspection PhpUnused
     */
    public function validationErrorKeyPresented(string $key, bool $strict = true): bool {

        if ($this->virtualUniqErrorKeys === null) { return false; }
        foreach ($this->virtualUniqErrorKeys as $virtualUniqErrorKey) {
            if ($strict && $virtualUniqErrorKey === $key) { return true; }
            else if (!$strict && str_contains($virtualUniqErrorKey, $key)) { return true; }
        }
        return false;
    }

    /**
     * @throws AbortException
     * @throws GraphQLSimpleCodeException
     */
    public function autoResponseReport(string $messageTranslationKey, bool $sendNow = true, bool $alert = false, bool $closeModal = false): void {

        if ($this->internalFrontHandler === null) {
            throw new GraphQLSimpleCodeException('Missing frontend handler for autoResponseReport!');
        }

        $translationKeyFromUniqErrorKey = $this->translationProvidedInAutoResponseReportUniqCodeMapping();

        try {
            $this->internalFrontHandler->frontHandler->handleApiResponse(
                responsePayload: new ResponsePayload(
                    message: $this->internalFrontHandler->frontHandler->translateKey($translationKeyFromUniqErrorKey ?? $messageTranslationKey),
                    type: $alert ? ResponsePayloadTypeEnum::TYPE_ALERT : ResponsePayloadTypeEnum::TYPE_ERROR,
                    closeModal: $closeModal,
                    uniqKeys: array_merge(['API'], $this->virtualUniqErrorKeys),
                    sentryEventId: $this->sentryEventId
                ),
                sendNow: $sendNow
            );
        } catch (InvalidArgument $e) {
            throw new GraphQLSimpleCodeException("Error translating key: {$messageTranslationKey}", $e);
        }
    }

    /**
     * Return uniq key mapping translation or null on undefined
     * @return string|null - translation key
     */
    private function translationProvidedInAutoResponseReportUniqCodeMapping(): string|null {

        $mapping = $this->internalFrontHandler->customAutoResponsePayloadUniqCodeMapping;
        $uniqKey = $this->virtualUniqErrorKeys !== null && count($this->virtualUniqErrorKeys) > 0 ? $this->virtualUniqErrorKeys[0] : null;
        if ($mapping === null || $uniqKey === null) { return null; }
        return $mapping[$uniqKey] ?? null;
    }
}
