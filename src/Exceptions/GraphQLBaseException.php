<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Exceptions;

use Exception;
use Throwable;

abstract class GraphQLBaseException extends Exception {

    protected string $callerFunction;

    protected int $callerLine;

    public function __construct(?string $message = null, ?Throwable $previous = null, ?int $code = null) {

        $this->callerFunction = debug_backtrace()[1]['function'];
        $this->callerLine = debug_backtrace()[1]['line'] ?? 0;

        parent::__construct($message ?? "GraphQL LIB exception on function {$this->callerFunction} and line {$this->callerLine}",
            $code ?? $previous?->getCode() ?? 0, $previous);
    }
}