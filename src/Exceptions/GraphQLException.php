<?php

declare(strict_types=1);

namespace GraphQLFrontApi\Exceptions;

use GraphQLFrontApi\Communication\Entities\SendGraphQLRequestParameters;
use GraphQLFrontApi\Communication\FrontHandlers\InternalFrontHandlerWrapper;
use GraphQLFrontApi\Debug\DebugObject;
use GraphQLFrontApi\Entities\ApiParsedError;
use GraphQLFrontApi\Entities\ApiParsedErrorLocation;
use GraphQLFrontApi\Entities\ApiParsedErrorViolation;
use Nette\Application\AbortException;
use Sentry\EventId;
use Throwable;
use Tracy\Debugger;

/**
 * Class GraphQLException
 * Used for any exceptions
 * @package GraphQLFrontApi
 */
abstract class GraphQLException extends GraphQLBaseException {

    /** @var ApiParsedError[]|null */
    public ?array $errors = null;

    /**
     * Custom client side uniq key for checking error codes
     * @var string[]|null
     */
    public ?array $virtualUniqErrorKeys = null;

    public ?DebugObject $debugObject = null;

    private ?array $apiResponse;

    private ?Throwable $previous;

    protected ?InternalFrontHandlerWrapper $internalFrontHandler = null;

    protected ?EventId $sentryEventId = null;

    /**
     * GraphQLExtendedException constructor.
     * @param DebugObject|null $debugObject
     * @param Throwable|null $previous
     * @param string|null $message
     * @param int|null $code
     * @throws GraphQLCodeException
     */
    public function __construct(?DebugObject $debugObject = null, ?string $message = null, ?Throwable $previous = null, ?int $code = null) {

        $this->previous = $previous;
        $this->debugObject = $debugObject;
        $this->internalFrontHandler = $debugObject->internalFrontHandler;

        $this->apiResponse = $debugObject->requestDebug->resultArray;
        $this->parseErrorSimple();
        $this->parseMultiplyErrors();
        $this->debugObject->virtualUniqErrorKeys = $this->virtualUniqErrorKeys;
        $this->debugObject->errors = $this->errors;

        parent::__construct(
            message: $message ?? $this->getExceptionMessage(),
            previous: $previous,
            code: $code ?? $this->getExceptionCode());

        if ($debugObject->parameters instanceof SendGraphQLRequestParameters) {
            Debugger::barDump($debugObject, $debugObject->parameters->query);
        } else {
            Debugger::barDump($debugObject, $debugObject->parameters->endpoint);
        }

        $this->sentryEventId = $this->logException();
    }

    /**
     * Log exception into file and mattermost service
     * @throws GraphQLCodeException
     * @return EventId|null - sentry event id or null
     */
    private function logException(): EventId|null {

        if ($this->debugObject->logger === null) {
            throw new GraphQLCodeException(message: 'You have to initialize logger first!', debugObject: $this->debugObject);
        }
        return $this->debugObject->logger->generateLogFromException($this);
    }

    /**
     * Try to get some exception code
     * @return int
     */
    private function getExceptionCode(): int {

        $previousExceptionCode = $this->previous?->getCode();
        $curlCode = $this->debugObject->requestDebug->responseHttpCode !== 200 ? $this->debugObject->requestDebug->responseHttpCode : null;
        return $curlCode ?? $this->errors[0]->status ?? $previousExceptionCode ?? 400;
    }

    /**
     * Try to find some exception message
     * @return string
     */
    private function getExceptionMessage(): string {

        $callerFunctionName = ucfirst(debug_backtrace()[2]['function']);
        $callerFunctionName = str_contains($callerFunctionName, '__construct') ? ucfirst(debug_backtrace()[3]['function']) : $callerFunctionName;
        $totalErrors = $this->errors === null ? 0 : count($this->errors);
        if ($this->errors === null) {
            return "{$callerFunctionName} throws exception! Please see debug object.";
        }
        if (count($this->errors) > 1) {
            return "{$callerFunctionName} throws exception with {$totalErrors} errors! Please see debug object.";
        }

        $canonized = $this->debugObject?->canonizedIdentify ?? null;

        // Ideal case: [GQL] Query: bill - ServiceException_NOT_EXISTS
        return '[GQL] ' .
            ($canonized === null ? '' : "{$canonized} - ") .
            ($this->debugObject->virtualUniqErrorKeys[0]
                ?? $this->errors[0]->message
                ?? $this->errors[0]->descriptionError
                ?? $this->errors[0]->error
                ?? $this->errors[0]->errorType
                ?? $this->errors[0]->classification
                ?? "{$callerFunctionName} throws exception without message! Please see debug object.");
    }

    private function parseErrorSimple(): void {

        if (!isset($this->apiResponse['error'])) { return; }
        $error = new ApiParsedError();
        $error->error = $this->apiResponse['error'];
        $error->message = $this->apiResponse['message'] ?? null;
        $error->paths = isset($this->apiResponse['path']) ? [$this->apiResponse['path']] : null;
        $error->timeStamp = $this->apiResponse['timestamp'] ?? null;
        $error->httpStatus = $this->apiResponse['status'] ?? null;
        $this->errors[] = $error;
    }

    private function parseMultiplyErrors(): void {

        if (!isset($this->apiResponse['errors'])) { return; }
        $this->virtualUniqErrorKeys = [];
        foreach ($this->apiResponse['errors'] as $errorResponse) {

            // Flat parameters
            $error = new ApiParsedError();
            $error->message = $errorResponse['message'] ?? null;
            $error->paths = $errorResponse['path'] ?? null;

            // Extensions
            if (isset($errorResponse['extensions'])) {
                $error->httpStatus = isset($errorResponse['extensions']['httpStatus']) ? (int) $errorResponse['extensions']['httpStatus'] : null;
                $error->objectClass = $errorResponse['extensions']['objectClass'] ?? null;
                $error->classification = $errorResponse['extensions']['classification'] ?? null;
                $error->errorType = $errorResponse['extensions']['errorType'] ?? null;
                $error->descriptionError = $errorResponse['extensions']['descriptionError'] ?? null;
            }

            // Locations
            if (isset($errorResponse['locations'])) {
                $error->locations = $this->parseExceptionLocations($errorResponse['locations']);
            }

            // Violations
            if (isset($errorResponse['extensions']['violations'])) {
                $error->violations = $this->parseExceptionViolations($errorResponse['extensions']['violations']);
            }

            $this->virtualUniqErrorKeys = array_merge($this->virtualUniqErrorKeys, $this->getVirtualUniqKeysForError($error));
            $this->errors[] = $error;
        }
    }

    /**
     * Parse locations in exception
     * @param array $errorResponseLocations
     * @return ApiParsedErrorLocation[]
     */
    private function parseExceptionLocations(array $errorResponseLocations): array {
        return array_map(static function ($location) {
            $outLocation = new ApiParsedErrorLocation();
            $outLocation->column = $location['column'] ?? null;
            $outLocation->line = $location['line'] ?? null;
            return $outLocation;
        }, $errorResponseLocations);
    }

    /**
     * Parse violations in exception
     * @param array $errorResponseViolations
     * @return array
     */
    private function parseExceptionViolations(array $errorResponseViolations): array {
        return array_map(static function (array $violation) {
            $outViolation = new ApiParsedErrorViolation();
            $outViolation->entityClassName = $violation['entityClassName'] ?? null;
            $outViolation->type = $violation['type'] ?? null;
            $outViolation->parameter = $violation['parameter'] ?? null;
            $outViolation->uniqueKey = $violation['uniqueKey'] ?? null;
            $outViolation->arguments = isset($violation['arguments']) && strlen($violation['arguments']) > 0 ? $violation['arguments'] : null;
            return $outViolation;
        }, $errorResponseViolations);
    }

    private function getVirtualUniqKeysForError(ApiParsedError $error): array|null {

        // Get base part of virtual key string
        $baseUniqKeyFragments = [];
        if ($error->objectClass !== null) { $baseUniqKeyFragments[] = $error->objectClass; }
        if ($error->errorType !== null) { $baseUniqKeyFragments[] = $error->errorType; }

        // Get uniq keys from violations
        $violationKeys = $error->violations !== null
            ? array_filter(
                array_map(static function(ApiParsedErrorViolation $violation) {
                    $violationFragments = [];
                    if ($violation->entityClassName !== null) { $violationFragments[] = $violation->entityClassName; }
                    if ($violation->parameter !== null) { $violationFragments[] = $violation->parameter; }
                    if ($violation->type !== null) { $violationFragments[] = $violation->type; }
                    if ($violation->uniqueKey !== null) { $violationFragments[] = $violation->uniqueKey; }
                    return $violationFragments;
                }, $error->violations), static function(array $violationFragments) { return count($violationFragments) !== 0; })
            : [];

        // If no violation uniqueKey and no objectClass or errorType return null
        if (count($baseUniqKeyFragments) === 0 && count($violationKeys) === 0) {
            return null;
        }

        // We don't have violation uniq keys
        if (count($violationKeys) === 0) {
            return [join('_', $baseUniqKeyFragments)];
        }

        // Produce string for every violation
        $outputVirtualKeys = [];
        foreach ($violationKeys as $violationKey) {
            $localFragments = array_merge($baseUniqKeyFragments, $violationKey);
            $outputVirtualKeys[] = preg_replace('/\s+/', '', join('_', $localFragments));
        }

        return $outputVirtualKeys;
    }

    /**
     * Auto report payload to FE
     * @param string $messageTranslationKey - key to translation
     * @param bool $sendNow
     * @throws GraphQLSimpleCodeException
     * @throws AbortException
     */
    public abstract function autoResponseReport(string $messageTranslationKey, bool $sendNow = true): void;
}