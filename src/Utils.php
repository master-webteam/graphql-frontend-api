<?php

declare(strict_types=1);

namespace GraphQLFrontApi;

use GraphQLFrontApi\Communication\Entities\SendGraphQLRequestParameters;
use GraphQLFrontApi\Debug\DebugObject;
use GraphQLFrontApi\Entities\ApiParsedError;
use GraphQLFrontApi\Entities\ApiParsedErrorViolation;
use GraphQLFrontApi\Exceptions\GraphQLSimpleCodeException;
use Sentry\State\Scope;

/**
 * Class Utils
 * Contains some useful functions
 * @package GraphQLFrontApi
 */
class Utils {

    public static function reportWarning(string $message): void {

        echo "\033[1;33m" . $message . "!\033[0m \n";
    }

    public static function reportSuccess(string $message): void {

        echo "\033[1;32m" . $message . ".\033[0m \n";
    }

    public static function reportInfo(string $message): void {

        echo "\033[1;34m" . $message . "...\033[0m \n";
    }

    public static function reportFinish(string $message): void {

        echo "\033[42m" . $message . "...\033[0m \n";
    }

    /**
     * Used to identify query in reporting tools
     * Fallback is base query - prevent errors
     * @param string $query - input query
     * @return string - query identification
     * @example mutation { deleteService(serviceId: 2), anotherFunction(serviceId: 3) } -> Mutation: deleteService, anotherFunction
     */
    public static function getSentryPerformanceIdentify(string $query): string {

        // Get type of query
        $re = '/(query|mutation)/m';
        preg_match_all($re, $query, $matches, PREG_SET_ORDER);
        if (!isset($matches[0][1])) { return $query; }
        $type = $matches[0][1];

        // Find query names
        $queryWithoutType = str_replace($type, '', $query);
        $queryWithoutStrings = preg_replace('/\".*?\"/m', '""', $queryWithoutType);
        $re = '/(\w+)(?:\([\S\s]+?\))?(?:\{[\w{}\s]+})?/m';
        preg_match_all($re, $queryWithoutStrings, $matches, PREG_SET_ORDER);
        if (!isset($matches[0][1])) { return $query; }

        // Compose
        $queryNames = '';
        foreach ($matches as $match) { $queryNames .= $match[1] . ', '; }
        return ucfirst($type) . ': ' . trim(trim($queryNames), ',');
    }

    /**
     * Get name of the query from whole query string
     * @param string $query - whole query
     * @return string - query name or exception on error
     * @throws GraphQLSimpleCodeException
     * @example deleteService(serviceId: 1000955) -> deleteService
     */
    public static function getQueryName(string $query): string {

        $re = '/([a-zA-Z]+).*/m';
        preg_match_all($re, $query, $matches, PREG_SET_ORDER);
        return $matches[0][1] ?? throw new GraphQLSimpleCodeException(message: "Impossible to parse query name: {$query}!");
    }

    /**
     * Remove indexing from query name
     * Used in reflections in multi-query result parsing
     * @param string $queryName - whole query
     * @return string - query name without indexing
     * @example deleteService1 -> deleteService
     */
    public static function removeIndexFromQueryName(string $queryName): string {
        return preg_replace('/[0-9]+/', '', $queryName);
    }

    /**
     * Function to convert query to named query
     * Used in mutations with multi-query
     * @param string $query - current query
     * @param int $index - index to append to named argument
     * @return string - updated query or exception on error
     * @throws GraphQLSimpleCodeException
     * @example deleteService(serviceId: 1000955) -> deleteService1: deleteService(serviceId: 1000955)
     */
    public static function getNamedByIndexQuery(string $query, int $index): string {

        $queryName = self::getQueryName($query);
        return "{$queryName}{$index}: {$query}";
    }

    /**
     * Update Sentry scope with extra info
     * @param Scope $scope
     * @param DebugObject $debugObject
     */
    public static function updateScopeFromDebugObject(Scope $scope, DebugObject $debugObject): void {

            if ($debugObject->userDebug->userId !== null) {
                $scope->setUser([
                    'id' => $debugObject->userDebug->userId,
                    'token' => $debugObject->userDebug->userToken,
                    'data' => $debugObject->userDebug->userData]);
            }

            if ($debugObject->parameters instanceof SendGraphQLRequestParameters) {
                $scope->setExtra('GraphQL query', $debugObject->parameters->query);
            } else {
                $scope->setExtra('Endpoint', $debugObject->parameters->endpoint);
            }

            $scope->setExtra('Virtual unique error keys', $debugObject->virtualUniqErrorKeys);
            $scope->setExtra('Canonized identify', $debugObject->canonizedIdentify);
            $scope->setExtra('API response', $debugObject->requestDebug->resultArray);
            $scope->setExtra('Input parameters', $debugObject->parameters);
            $scope->setExtra('Parsed errors', $debugObject->errors);
            $scope->setExtra('Communication debug', $debugObject->requestDebug);
            $scope->setExtra('Redis cache debug', $debugObject->cacheDebug);
            $scope->setExtra('Library debug', $debugObject->communicatorDebug);
            $scope->setExtra('Identity debug', $debugObject->userDebug->userData);
            $scope->setExtra('Request body', $debugObject->request?->getRawBody());
    }

    /**
     * Convert http code to message
     * @param int $code - http code
     * @return string - translated message
     */
    public static function httpCodeToMessage(int $code): string {

        return match ($code) {
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Moved Temporarily',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Time-out',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Large',
            415 => 'Unsupported Media Type',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Time-out',
            505 => 'HTTP Version not supported',
            default => "Unexpected {$code}",
        };
    }
}