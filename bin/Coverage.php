<?php

namespace GraphQLFrontApi;

use JetBrains\PhpStorm\NoReturn;
use JsonException;
use RuntimeException;

/**
 * Contains script to get coverage between generated repos and used code
 */
class Coverage {

    private array $sourcePaths = [];

    private array $testsPaths = [];

    private array $usedProperties = [];

    private ?string $coverageFilePath = null;

    private ?string $coverageConfigPath = null;

    private array $ignoreMethods = [];

    public function addSourcePath(string $soucePath): void {
        $this->sourcePaths[] = $soucePath;
    }

    public function addTestsPath(string $testsPath): void {
        $this->testsPaths[] = $testsPath;
    }

    public function setCoverageFilePath(string $coverageFilePath): void {
        $this->coverageFilePath = $coverageFilePath;
    }

    public function setCoverageConfigPath(string $coverageConfigPath): void {
        $this->coverageConfigPath = $coverageConfigPath;
    }

    public function addUsedProperty(string $usedProperty): void {
        $this->usedProperties[] = $usedProperty;
    }

    /**
     * Return error and show help for the user
     * @noinspection RequiredAttributes
     */
    #[NoReturn] public static function showHelp(): void {

        echo "Usage: php Coverage.php <source1> <sourceN> <tests1> <testsN> <property1> <propertyN> [file]\n";
        echo "--source <path>    Code source containing function calls\n";
        echo "--tests <path>     Test cases source code containing test methods\n";
        echo "--file <path>      Coverage file with percent count\n";
        echo "--property <name>  Property name used to call generated functions\n";
        echo "--config <path>    Set config file for coverage script\n";
        exit(1);
    }

    /**
     * Main run function with control flow
     */
    public function run(): void {

        $this->checkCoverageConfigIfSet();
        $sourcePaths = $this->getListOfAcceptedFiles($this->sourcePaths);
        $sourceFunctions = $this->getListOfMethodsUsedInFiles($sourcePaths);
        $testsPaths = $this->getListOfAcceptedFiles($this->testsPaths);
        $testsFunctions = $this->getListOfMethodsUsedInFiles($testsPaths);

        $missingTest = array_values(array_diff($sourceFunctions, $testsFunctions));
        $unusedTest = array_values(array_diff($testsFunctions, $sourceFunctions));
        if (!empty($missingTest)) { self::printMethods($missingTest, 'Missing test case for method', 'red'); }
        if (!empty($unusedTest)) { self::printMethods($unusedTest, 'Method tested but not used','orange'); }

        $usedCount = count($sourceFunctions);
        $testedCount = count($testsFunctions);
        $missingCount = count($missingTest);
        $coveragePct = (int) (100 - ($missingCount / $usedCount * 100));
        self::printSummary($usedCount, $testedCount, $missingCount, $coveragePct);

        if ($this->coverageFilePath !== null) {
            $this->saveCoveragePercentToFile($coveragePct);
        }
    }

    private function checkCoverageConfigIfSet() : void {

        if ($this->coverageConfigPath !== null) {
            if (!is_file($this->coverageConfigPath)) { self::showHelp(); }
            $configContent = file_get_contents($this->coverageConfigPath);
            try { $configContentParsed = json_decode($configContent, false, 512, JSON_THROW_ON_ERROR); }
            catch (JsonException) { self::showHelp(); }
            if (!property_exists($configContentParsed, 'coverage')) { self::showHelp(); }
            if (property_exists($configContentParsed->coverage, 'ignoredMethods')) {
                $this->ignoreMethods = $configContentParsed->coverage->ignoredMethods;
            }
        }
    }

    /**
     * Iterate through source paths and collect all PHP files
     * @noinspection SlowArrayOperationsInLoopInspection
     */
    private function getListOfAcceptedFiles(array $directoryPaths) : array {

        $output = [];
        foreach ($directoryPaths as $directoryPath) {
            $directoryPathDirs = self::recursiveDirectoryIteratorForDirectories($directoryPath);
            foreach ($directoryPathDirs as $directoryPathDir) {
                $output = array_merge($output, self::recursiveDirectoryIteratorForFiles($directoryPathDir));
            }
        } return array_unique($output);
    }

    /**
     * Get list of all directories using recursion
     * @param string $directoryPath - current directory
     * @return array - list of directories with path
     * @noinspection SlowArrayOperationsInLoopInspection
     */
    private static function recursiveDirectoryIteratorForDirectories(string $directoryPath) : array {

        $output = [$directoryPath];
        $sourcePathDirs = array_map(static function($directory) use ($directoryPath) { return $directoryPath . DIRECTORY_SEPARATOR . $directory; },
            array_filter(scandir($directoryPath), static function ($file) use ($directoryPath) {
                return !in_array($file, ['.', '..']) && is_dir($directoryPath . DIRECTORY_SEPARATOR . $file); }));

        foreach ($sourcePathDirs as $sourcePathDir) {
            $sourcePathDirsRecursive = self::recursiveDirectoryIteratorForDirectories($sourcePathDir);
            $output = array_merge($output, $sourcePathDirsRecursive);
        } return array_merge($output, $sourcePathDirs);
    }

    /**
     * Get list of all PHP files in the directory
     * @param string $directoryPath - directory path
     * @return array - list of all PHP files
     */
    private static function recursiveDirectoryIteratorForFiles(string $directoryPath) : array {

        return array_map(static function ($file) use ($directoryPath) { return $directoryPath . DIRECTORY_SEPARATOR . $file; },
            array_filter(scandir($directoryPath), static function ($file) { return str_contains($file, '.php'); }));
    }

    /**
     * Lookup in file and try to search function usages
     * @noinspection SlowArrayOperationsInLoopInspection
     */
    private function getListOfMethodsUsedInFiles(array $filesList) : array {

        $usedPropertiesRegexFragment = '';
        foreach ($this->usedProperties as $usedProperty) {
            $usedPropertiesRegexFragment .= "$usedProperty|";
        } $usedPropertiesRegexFragment = rtrim($usedPropertiesRegexFragment, '|');

        $output = [];
        foreach ($filesList as $file) {
            $fileContent = file_get_contents($file);
            $regexString = "/($usedPropertiesRegexFragment)->(.+?)\(/ms";
            preg_match_all($regexString, $fileContent, $matches, PREG_PATTERN_ORDER);
            if (!isset($matches[2])) { continue; }
            $output = array_merge($output, $matches[2]);
        }

        return array_unique(array_diff($output, $this->ignoreMethods));
    }

    private static function printMethods(array $methods, string $title, string $color = 'white') : void {

        $colorString = match ($color) {
            'red' => "\033[31m",
            'orange' => "\033[33m",
            default => "\033[97m",
        };

        echo $colorString;
        for ($i = 0; $i < 30; $i++) { echo '-'; } echo "\n";
        echo "$title\n";
        for ($i = 0; $i < 30; $i++) { echo '-'; } echo "\n";
        foreach ($methods as $method) { echo "$method\n"; }
        echo "\n";
    }

    private static function printSummary(int $usedCount, int $testedCount, int $missingCount, int $coveragePct) : void {

        echo "\033[34m";
        for ($i = 0; $i < 30; $i++) { echo '-'; } echo "\n";
        echo "Summary\n";
        for ($i = 0; $i < 30; $i++) { echo '-'; } echo "\n";
        echo "Total number of used functions: $usedCount\n";
        echo "Total number of tested functions: $testedCount\n";
        echo "Need to be tested: $missingCount\n";
        echo "Total $coveragePct% covered.\n";
    }

    private function saveCoveragePercentToFile(int $coveragePercent) : void {

        if (!file_exists(dirname($this->coverageFilePath))
            && !mkdir($concurrentDirectory = dirname($this->coverageFilePath), true) && !is_dir($concurrentDirectory)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        } file_put_contents($this->coverageFilePath, "<coveredPercent>$coveragePercent</coveredPercent>");
    }
}

// Parse input parameters
$app = new Coverage();
if ($argc < 5) { Coverage::showHelp(); }
for ($i = 1; $i < $argc; $i += 2) {
    if (!isset($argv[$i], $argv[$i + 1])) { echo "Wrong arguments\n"; Coverage::showHelp(); }
    if ($argv[$i] === '--source') { $app->addSourcePath($argv[$i+1]); }
    else if ($argv[$i] === '--tests'){ $app->addTestsPath($argv[$i+1]); }
    else if ($argv[$i] === '--file'){ $app->setCoverageFilePath($argv[$i+1]); }
    else if ($argv[$i] === '--property'){ $app->addUsedProperty($argv[$i+1]); }
    else if ($argv[$i] === '--config'){ $app->setCoverageConfigPath($argv[$i+1]); }
    else { echo "Wrong argument: $argv[$i]\n"; Coverage::showHelp(); }
}

// Run application
$app->run();
